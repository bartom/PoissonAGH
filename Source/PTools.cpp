#include "PoissonAGH/Utils/PTools.hpp"
#include <string>


namespace PoissonAGH
{
using namespace std::string_literals;

std::string toString(MultigridCycle cycle)
{
    switch (cycle)
    {
        case MultigridCycle::VCycle: return "VCycle"s;
        case MultigridCycle::WCycle: return "WCycle"s;
        case MultigridCycle::FCycle: return "FCycle"s;
        case MultigridCycle::FullMultigrid: return "Full multigrid"s;
    }

    __builtin_unreachable();
}

std::ostream& operator<<(std::ostream& os, MultigridCycle cycle)
{
    os << toString(cycle);
    return os;
}

void fillArrayWith1DFunction(Array1D& array, Function1D function, GridStep step, double rangeStart,
                             size_t staringIndex)
{
    std::generate(std::begin(array), std::end(array), [&]() {
        return function(staringIndex++ *  step + rangeStart);
    });
}
}
