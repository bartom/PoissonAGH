#include <PoissonAGH/Poisson1D.hpp>
#include "PoissonAGH/Poisson1D_C.h"
#include "PoissonAGH/Poisson1D.hpp"

using namespace PoissonAGH;

#ifdef WIP
auto P1D_reinterpret = [](Poisson1D_Ptr ptr){ return reinterpret_cast<Poisson1D*>(ptr); };

Poisson1D_Ptr P1D_new_Poisson1D_step(double xmin, double xmax, double h, PFunction1D densityFunc)
{
     return reinterpret_cast<Poisson1D_Ptr>(new Poisson1D(AxisXRange{xmin, xmax}, GridStep(h), densityFunc));
}

Poisson1D_Ptr P1D_new_Poisson1D_size(double xmin, double xmax, size_t nx, PFunction1D densityFunc)
{
     return reinterpret_cast<Poisson1D_Ptr>(new Poisson1D({xmin, xmax}, DimensionSize(nx), densityFunc));
}

Poisson1D_Ptr P1D_new_Poisson1D_array(double* rho_arr, size_t len, double h)
{
     Array1D vals(rho_arr, rho_arr+len);
     return reinterpret_cast<Poisson1D_Ptr>(new Poisson1D(vals, GridStep(h)));
}

void P1D_delete_Poisson1D(Poisson1D_Ptr solver)
{
     delete P1D_reinterpret(solver);
}

int P1D_isSolved(Poisson1D_Ptr solver)
{
     return P1D_reinterpret(solver)->isSolved();
}

double P1D_getXmin(Poisson1D_Ptr solver)
{
     return P1D_reinterpret(solver)->axisX().min;
}

double P1D_getXmax(Poisson1D_Ptr solver)
{
     return P1D_reinterpret(solver)->axisX().max;
}

size_t P1D_getNx(Poisson1D_Ptr solver)
{
     return P1D_reinterpret(solver)->nX().value();
}

double P1D_getStep(Poisson1D_Ptr solver)
{
     return P1D_reinterpret(solver)->gridStep().value();
}

double P1D_getEpsilonC(Poisson1D_Ptr solver)
{
     return P1D_reinterpret(solver)->epsilonC.value();
}

void P1D_setEpsilonC(Poisson1D_Ptr solver, double epsilonC)
{
     P1D_reinterpret(solver)->epsilonC = PhysicalConstant(epsilonC);
}

void P1D_setEpsilonArray(Poisson1D_Ptr solver, double* eps_arr)
{
    Poisson1D* ptr = P1D_reinterpret(solver);
    Array1D vals(eps_arr, eps_arr+ptr->nX().value());
    ptr->setEpsilonArray(vals);
}

const double* P1D_getEpsilonArray(Poisson1D_Ptr solver)
{
     return P1D_reinterpret(solver)->epsilonArr.data();
}

const double* P1D_getPhiArray(Poisson1D_Ptr solver)
{
     return P1D_reinterpret(solver)->phi.data();
}

const double* P1D_getRhoArray(Poisson1D_Ptr solver)
{
     return P1D_reinterpret(solver)->rho.data();
}

void P1D_setBoundaryValues(Poisson1D_Ptr solver, double left, double right)
{
     Poisson1D* ptr = P1D_reinterpret(solver);
     ptr->boundary.left = PhysicalConstant(left);
     ptr->boundary.right = PhysicalConstant(right);
}

double P1D_getLeftBoundaryValue(Poisson1D_Ptr solver)
{
     return P1D_reinterpret(solver)->boundary.left;
}
double P1D_getRightBoundaryValue(Poisson1D_Ptr solver)
{
     return P1D_reinterpret(solver)->boundary.right;
}

void P1D_setConditionsType(Poisson1D_Ptr solver, P1D_ConditionType conditionType)
{
     auto condition = static_cast<Poisson1D::ConditionType>(conditionType);
     P1D_reinterpret(solver)->condition = condition;
}

P1D_ConditionType P1D_getConditionType(Poisson1D_Ptr solver)
{
     return static_cast<P1D_ConditionType>(P1D_reinterpret(solver)->condition);
}

void P1D_solve(Poisson1D_Ptr solver)
{
     P1D_reinterpret(solver)->solve();
}

#endif

