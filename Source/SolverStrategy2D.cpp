#include "PoissonAGH/Solver/SolverStrategy2D.hpp"

namespace PoissonAGH
{
namespace utils
{

using namespace std;

void AllDirichletGSConstEps2D::gaussSeidelOneColour(IGaussSeidelSolverStrategy2D::Colour colour) noexcept
{
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 2 - (i + colour) % 2; j <= cols; j += 2)
        {
            u(i, j) = 0.25 * (u(i-1, j) + u(i+1, j) + u(i, j-1) + u(i, j+1) + h.value()*h.value()*f(i, j)/eps);
        }
    }
}

void AllDirichletGSConstEps2D::solve() noexcept
{
    gaussSeidelOneColour(Colour::RED);
    gaussSeidelOneColour(Colour::BLACK);
}

void AllDirichletGSConstEps2D::findResidual(Matrix2D& r) noexcept
{
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            r(i, j) += eps*(u(i+1, j) + u(i-1, j) + u(i, j+1) + u(i, j-1) - 4*u(i, j)) / (h.value()*h.value());
        }
    }
}

double AllDirichletGSConstEps2D::findSumResidual() noexcept
{
    double sum = 0;
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            sum += f(i, j) + eps*(u(i+1, j) + u(i-1, j) + u(i, j+1) + u(i, j-1) - 4*u(i, j)) / (h.value()*h.value());
        }
    }
    return sum;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

NeumannSolverGSConstEps2D::NeumannSolverGSConstEps2D(bool northNeumann, bool southNeumann, bool westNeumann,
                                                     bool eastNeumann, Matrix2D& u, const Matrix2D& f, GridStep h,
                                                     double eps):
    u(u), f(f), h(h), eps(eps), rows(u.rows()-2), cols(u.cols()-2),
    hMinNorth(northNeumann ? 2 : 1.),
    hMaxNorth(northNeumann ? 2*h.value() : 1.),
    hMinSouth(southNeumann ? -2*h.value() : 1.),
    hMaxSouth(southNeumann ? 2 : 1.),
    hMinWest(westNeumann ? -2*h.value() : 1.),
    hMaxWest(westNeumann ? 2 : 1.),
    hMinEast(eastNeumann ? 2 : 1.),
    hMaxEast(eastNeumann ? 2*h.value() : 1)
{
}


inline void NeumannSolverGSConstEps2D::solveDirichlet(Matrix2D& u, size_t i, size_t j) const noexcept
{
    u(i, j) = 0.25 * (u(i-1, j) + u(i+1, j) + u(i, j-1) + u(i, j+1) + h.value()*h.value()*f(i, j)/eps);
}

inline void NeumannSolverGSConstEps2D::solveNeumann(Matrix2D& u, size_t i, size_t j, GridStep hXmin, GridStep hXmax,
                                                    GridStep hYmin, GridStep hYmax) const noexcept
{
    u(i, j) = 0.25 * (hXmin.value()*u(i-1, j) + hXmax.value()*u(i+1, j) + hYmin.value()*u(i, j-1) +
            hYmax.value()*u(i, j+1) + h.value()*h.value()*f(i, j)/eps);
}

void NeumannSolverGSConstEps2D::gaussSeidelOneColour(Colour colour) noexcept
{
    generalSolverTemplate(&NeumannSolverGSConstEps2D::solveNeumann, &NeumannSolverGSConstEps2D::solveDirichlet, u, colour);
}

void NeumannSolverGSConstEps2D::solve() noexcept
{
    gaussSeidelOneColour(Colour::RED);
    gaussSeidelOneColour(Colour::BLACK);
}

inline void NeumannSolverGSConstEps2D::findRDirichlet(Matrix2D& r, size_t i, size_t j) const noexcept
{
    r(i, j) += eps*(u(i+1, j) + u(i-1, j) + u(i, j+1) + u(i, j-1) - 4*u(i, j)) / (h.value()*h.value());
}

inline void NeumannSolverGSConstEps2D::findRNeumann(Matrix2D& r, size_t i, size_t j, GridStep hXmin, GridStep hXmax, GridStep hYmin,
                                             GridStep hYmax) const noexcept
{
    r(i, j) += eps*(hXmin.value()*u(i-1, j) + hXmax.value()*u(i+1, j) + hYmin.value()*u(i, j-1) + hYmax.value()*u(i, j+1) - 4*u(i, j)) / (h.value()*h.value());
}


void NeumannSolverGSConstEps2D::findResidual(Matrix2D& r) noexcept
{
    generalSolverTemplate(&NeumannSolverGSConstEps2D::findRNeumann, &NeumannSolverGSConstEps2D::findRDirichlet, r, Colour::RED);
    generalSolverTemplate(&NeumannSolverGSConstEps2D::findRNeumann, &NeumannSolverGSConstEps2D::findRDirichlet, r, Colour::BLACK);
}

void NeumannSolverGSConstEps2D::findSumRDirichlet(double& r, size_t i, size_t j) const noexcept
{
    r += f(i, j) + eps*(u(i+1, j) + u(i-1, j) + u(i, j+1) + u(i, j-1) - 4*u(i, j)) / (h.value()*h.value());
}

void NeumannSolverGSConstEps2D::findSumRNeumann(double& r, size_t i, size_t j, GridStep hXmin, GridStep hXmax, GridStep hYmin,
                                                GridStep hYmax) const noexcept
{
    r += f(i, j) + eps*(hXmin.value()*u(i-1, j) + hXmax.value()*u(i+1, j) + hYmin.value()*u(i, j-1) + hYmax.value()*u(i, j+1) - 4*u(i, j)) / (h.value()*h.value());
}

double NeumannSolverGSConstEps2D::findSumResidual() noexcept
{
    double sum = 0;
    generalSolverTemplate(&NeumannSolverGSConstEps2D::findSumRNeumann, &NeumannSolverGSConstEps2D::findSumRDirichlet,
                          sum, Colour::RED);
    generalSolverTemplate(&NeumannSolverGSConstEps2D::findSumRNeumann, &NeumannSolverGSConstEps2D::findSumRDirichlet,
                          sum, Colour::BLACK);
    return sum;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AllDirichletGSVarEps2D::gaussSeidelOneColour(Colour colour) noexcept
{
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 2 - (i + colour) % 2; j <= cols; j += 2)
        {
            const double epsNI = eps(i+1, j) + eps(i, j);
            const double epsPI = eps(i-1, j) + eps(i, j);
            const double epsNJ = eps(i, j+1) + eps(i, j);
            const double epsPJ = eps(i, j-1) + eps(i, j);

            u(i, j) = (epsPI*u(i-1, j) + epsNI*u(i+1, j) + epsPJ*u(i, j-1) + epsNJ*u(i, j+1) + 2*h.value()*h.value()*f(i, j)) /
                      (epsNI + epsNJ + epsPI + epsPJ);
        }
    }
}

void AllDirichletGSVarEps2D::solve() noexcept
{
    gaussSeidelOneColour(Colour::RED);
    gaussSeidelOneColour(Colour::BLACK);
}

void AllDirichletGSVarEps2D::findResidual(Matrix2D& r) noexcept
{
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            const double epsNI = eps(i+1, j) + eps(i, j);
            const double epsPI = eps(i-1, j) + eps(i, j);
            const double epsNJ = eps(i, j+1) + eps(i, j);
            const double epsPJ = eps(i, j-1) + eps(i, j);

            r(i, j) += (epsNI*u(i+1, j) + epsPI*u(i-1, j) + epsNJ*u(i, j+1) + epsPJ*u(i, j-1) -
                        (epsNI + epsNJ + epsPI + epsPJ)*u(i, j)) / (2*h.value()*h.value());
        }
    }
}

double AllDirichletGSVarEps2D::findSumResidual() noexcept
{
    double sum = 0;

    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            const double epsNI = eps(i+1, j) + eps(i, j);
            const double epsPI = eps(i-1, j) + eps(i, j);
            const double epsNJ = eps(i, j+1) + eps(i, j);
            const double epsPJ = eps(i, j-1) + eps(i, j);

            sum += f(i, j) + (epsNI*u(i+1, j) + epsPI*u(i-1, j) + epsNJ*u(i, j+1) + epsPJ*u(i, j-1) -
                    (epsNI + epsNJ + epsPI + epsPJ)*u(i, j)) / (2*h.value()*h.value());
        }
    }

    return sum;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

NeumannSolverGSVarEps2D::NeumannSolverGSVarEps2D(bool isNorth, bool isSouth, bool isWest, bool isEast,
                                                 Matrix2D& u, const Matrix2D& f, GridStep h,
                                                 const Matrix2D& eps):
        u(u), f(f), h(h), eps(eps), rows(u.rows()-2), cols(u.cols()-2),
        northDirichlet(&NeumannSolverGSVarEps2D::createPartialDirichletSolver<1, 0>),
        southDirichlet(&NeumannSolverGSVarEps2D::createPartialDirichletSolver<-1, 0>),
        westDirichlet(&NeumannSolverGSVarEps2D::createPartialDirichletSolver<0, -1>),
        eastDirichlet(&NeumannSolverGSVarEps2D::createPartialDirichletSolver<0, 1>)
{
    northNeumann = isNorth ? &NeumannSolverGSVarEps2D::createPartialNeumannSolver<1, 0> : northDirichlet;
    southNeumann = isSouth ? &NeumannSolverGSVarEps2D::createPartialNeumannSolver<-1, 0> : southDirichlet;
    westNeumann = isWest ? &NeumannSolverGSVarEps2D::createPartialNeumannSolver<0, -1> : westDirichlet;
    eastNeumann = isEast ? &NeumannSolverGSVarEps2D::createPartialNeumannSolver<0, 1> : eastDirichlet;
}



inline void NeumannSolverGSVarEps2D::solveDirichlet(Matrix2D& u, size_t i, size_t j) noexcept
{
    const double epsNI = eps(i+1, j) + eps(i, j);
    const double epsPI = eps(i-1, j) + eps(i, j);
    const double epsNJ = eps(i, j+1) + eps(i, j);
    const double epsPJ = eps(i, j-1) + eps(i, j);
    u(i, j) = (epsPI*u(i-1, j) + epsNI*u(i+1, j) + epsPJ*u(i, j-1) + epsNJ*u(i, j+1) + 2*h.value()*h.value()*f(i, j)) /
                (epsNI + epsNJ + epsPI + epsPJ);
}

inline double NeumannSolverGSVarEps2D::getSumEps(size_t i, size_t j) const noexcept
{
    return 4*eps(i, j) + eps(i+1, j) + eps(i-1, j) + eps(i, j-1) + eps(i, j+1);
}


inline void NeumannSolverGSVarEps2D::gaussSeidelOneColour(IGaussSeidelSolverStrategy2D::Colour colour) noexcept
{
    generalSolverTemplate(&NeumannSolverGSVarEps2D::solveNeumann<PartialSolver>,
                          &NeumannSolverGSVarEps2D::solveDirichlet, u, colour);
}
void NeumannSolverGSVarEps2D::solve() noexcept
{
    gaussSeidelOneColour(Colour::RED);
    gaussSeidelOneColour(Colour::BLACK);
}

inline void NeumannSolverGSVarEps2D::findRDirichlet(Matrix2D& r, size_t i, size_t j) const noexcept
{
    const double epsNI = eps(i+1, j) + eps(i, j);
    const double epsPI = eps(i-1, j) + eps(i, j);
    const double epsNJ = eps(i, j+1) + eps(i, j);
    const double epsPJ = eps(i, j-1) + eps(i, j);
    r(i, j) += (epsNI*u(i+1, j) + epsPI*u(i-1, j) + epsNJ*u(i, j+1) + epsPJ*u(i, j-1) - (epsNI + epsNJ + epsPI + epsPJ)*u(i, j)) / (2*h.value()*h.value());
}

void NeumannSolverGSVarEps2D::findResidual(Matrix2D& r) noexcept
{
    generalSolverTemplate(&NeumannSolverGSVarEps2D::findRNeumann<PartialSolver>,
                          &NeumannSolverGSVarEps2D::findRDirichlet, r, Colour::RED);
    generalSolverTemplate(&NeumannSolverGSVarEps2D::findRNeumann<PartialSolver>,
                          &NeumannSolverGSVarEps2D::findRDirichlet, r, Colour::BLACK);
}

void NeumannSolverGSVarEps2D::findSumRDirichlet(double& r, size_t i, size_t j) const noexcept
{
    const double epsNI = eps(i+1, j) + eps(i, j);
    const double epsPI = eps(i-1, j) + eps(i, j);
    const double epsNJ = eps(i, j+1) + eps(i, j);
    const double epsPJ = eps(i, j-1) + eps(i, j);
    r += f(i, j) + (epsNI*u(i+1, j) + epsPI*u(i-1, j) + epsNJ*u(i, j+1) + epsPJ*u(i, j-1)
                    - (epsNI + epsNJ + epsPI + epsPJ)*u(i, j)) / (2*h.value()*h.value());
}

double NeumannSolverGSVarEps2D::findSumResidual() noexcept
{
    double sum = 0;
    generalSolverTemplate(&NeumannSolverGSVarEps2D::findSumRNeumann<PartialSolver>,
                          &NeumannSolverGSVarEps2D::findSumRDirichlet, sum, Colour::RED);
    generalSolverTemplate(&NeumannSolverGSVarEps2D::findSumRNeumann<PartialSolver>,
                          &NeumannSolverGSVarEps2D::findSumRDirichlet, sum, Colour::BLACK);
    return sum;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::unique_ptr<ISolverStrategy2D>
SolverGSStrategy2DFactory::createSolverConstEps(ConditionType2D north, ConditionType2D south, ConditionType2D west,
                                                ConditionType2D east, Matrix2D& u, const Matrix2D& f,
                                                GridStep h, double eps)
{
    if(north == ConditionType2D::DirichletNorth and south == ConditionType2D::DirichletSouth and
       west == ConditionType2D::DirichletWest and east == ConditionType2D::DirichletEast)
    {
        return make_unique<AllDirichletGSConstEps2D>(u, f, h, eps);
    }

    bool northNeumann = north == ConditionType2D::NeumannNorth;
    bool southNeumann = south == ConditionType2D::NeumannSouth;
    bool westNeumann = west == ConditionType2D::NeumannWest;
    bool eastNeumann = east == ConditionType2D::NeumannEast;

    if(not northNeumann and not southNeumann and not westNeumann and not eastNeumann)
    {
        throw std::logic_error("Wrong conditions types passed!");
    }

    return make_unique<NeumannSolverGSConstEps2D>(northNeumann, southNeumann, westNeumann, eastNeumann, u, f, h, eps);
}

std::unique_ptr<ISolverStrategy2D>
SolverGSStrategy2DFactory::createSolverVarEps(ConditionType2D north, ConditionType2D south, ConditionType2D west,
                                              ConditionType2D east, Matrix2D& u, const Matrix2D& f,
                                              GridStep h, const Matrix2D& eps)
{
    if(north == ConditionType2D::DirichletNorth and south == ConditionType2D::DirichletSouth and
       west == ConditionType2D::DirichletWest and east == ConditionType2D::DirichletEast)
    {
        return make_unique<AllDirichletGSVarEps2D>(u, f, h, eps);
    }

    bool northNeumann = north == ConditionType2D::NeumannNorth;
    bool southNeumann = south == ConditionType2D::NeumannSouth;
    bool westNeumann = west == ConditionType2D::NeumannWest;
    bool eastNeumann = east == ConditionType2D::NeumannEast;

    if(!northNeumann and !southNeumann and !westNeumann and !eastNeumann)
    {
        throw std::logic_error("Wrong conditions types passed!");
    }

    return make_unique<NeumannSolverGSVarEps2D>(northNeumann, southNeumann, westNeumann, eastNeumann, u, f, h, eps);
}

}

}

