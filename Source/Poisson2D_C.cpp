#include "PoissonAGH/Poisson2D_C.h"
#include "PoissonAGH/Poisson2D.hpp"

using namespace PoissonAGH;

//auto P2D_reinterpret = [](Poisson2D_Ptr ptr){ return reinterpret_cast<Poisson2D*>(ptr); };

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIP
Poisson2D_Ptr P2D_new_Poisson2D_squareStep(double xmin, double xmax, double h)
{
    return reinterpret_cast<Poisson2D_Ptr>(new Poisson2D(xmin, xmax, h));
}

Poisson2D_Ptr P2D_new_Poisson2D_squareSize(double xmin, double xmax, size_t nx)
{
    return reinterpret_cast<Poisson2D_Ptr>(new Poisson2D(xmin, xmax, nx));
}

Poisson2D_Ptr P2D_new_Poisson2D_rectangleStep(double xmin, double xmax, double ymin, double ymax, double h)
{
    return reinterpret_cast<Poisson2D_Ptr>(new Poisson2D(xmin, xmax, ymin, ymax, h));
}

Poisson2D_Ptr P2D_new_Poisson2D_matrix(double** rho_arr, size_t rows, size_t cols, double h)
{
    Matrix2D data(rho_arr, rows, cols);
    return reinterpret_cast<Poisson2D_Ptr>(new Poisson2D(std::move(data), h));
}

Poisson2D_Ptr P2D_new_Poisson2D_array(double* rho_arr, size_t rows, size_t cols, double h)
{
    Matrix2D data(rho_arr, rows, cols);
    return reinterpret_cast<Poisson2D_Ptr>(new Poisson2D(std::move(data), h));
}

void P2D_delete_Poisson2D(Poisson2D_Ptr solver)
{
    delete P2D_reinterpret(solver);
}

int P2D_isSolved(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->isSolved();
}

double P2D_getXmin(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->getXmin();
}

double P2D_getXmax(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->getXmax();
}

double P2D_getYmin(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->getYmin();
}

double P2D_getYmax(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->getYmax();
}

size_t P2D_getNx(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->getNx();
}
size_t P2D_getNy(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->getNy();
}

double P2D_getStep(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->getStep();
}

double P2D_getEpsilonC(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->epsilonConstant;
}

void P2D_setEpsilonC(Poisson2D_Ptr solver, double epsilonC)
{
    P2D_reinterpret(solver)->epsilonConstant = epsilonC;
}

const double* P2D_getEpsilonMatrix(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->epsilonMatrix.getData();
}

void P2D_setEpsilonMatrix(Poisson2D_Ptr solver, const double* eps_matrix)
{
    Poisson2D* ptr = P2D_reinterpret(solver);
    Matrix2D epsilon(eps_matrix, ptr->getNx()+2, ptr->getNy()+2);
    ptr->setEpsilonMatrix(std::move(epsilon));
}

const double* P2D_getPhiMatrix(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->phi.getData();
}

const double* P2D_getRhoMatrix(Poisson2D_Ptr solver)
{
    return P2D_reinterpret(solver)->rho.getData();
}

void P2D_setSolverSteps(Poisson2D_Ptr solver, size_t steps)
{
    P2D_reinterpret(solver)->solverSteps = steps;
}

void P2D_setMaxMultigridCalls(Poisson2D_Ptr solver, size_t calls)
{
    P2D_reinterpret(solver)->maxCalls = calls;
}

void P2D_setResidualDelta(Poisson2D_Ptr solver, double delta)
{
    P2D_reinterpret(solver)->residualDelta = delta;
}

void P2D_setCondition(Poisson2D_Ptr solver, const double* values, size_t len, P2D_ConditionType2D condition)
{
    Array1D data(values, values+len);
    P2D_reinterpret(solver)->setCondition(data, static_cast<ConditionType2D>(condition));
}

P2D_ConditionType2D P2D_getNorthCondition(Poisson2D_Ptr solver)
{
    return static_cast<P2D_ConditionType2D>(P2D_reinterpret(solver)->getNorthCondition());
}

P2D_ConditionType2D P2D_getSouthCondition(Poisson2D_Ptr solver)
{
    return static_cast<P2D_ConditionType2D>(P2D_reinterpret(solver)->getSouthCondition());
}

P2D_ConditionType2D P2D_getWestCondition(Poisson2D_Ptr solver)
{
    return static_cast<P2D_ConditionType2D>(P2D_reinterpret(solver)->getWestCondition());
}

P2D_ConditionType2D P2D_getEastCondition(Poisson2D_Ptr solver)
{
    return static_cast<P2D_ConditionType2D>(P2D_reinterpret(solver)->getEastCondition());
}

void P2D_setMultigridType(Poisson2D_Ptr solver, P2D_MultigridType multigridType)
{
    P2D_reinterpret(solver)->setMultigridCycle(static_cast<MultigridCycle>(multigridType));
}

void P2D_solve(Poisson2D_Ptr solver)
{
    P2D_reinterpret(solver)->solve();
}

double P2D_getValueAt(const double* matrix, size_t columns, size_t i, size_t j)
{
    return matrix[j + i*columns];
}

void P2D_setValueAt(double* matrix, double value, size_t columns, size_t i, size_t j)
{
    matrix[j + i*columns] = value;
}
#endif

#ifdef __cplusplus
}
#endif
