# disable these tests for now
if(FALSE)
add_executable(PoissonCAPITests
        Poisson1DAPITestSuite.cpp Poisson2DAPITestSuite.cpp Poisson3DAPITestSuite.cpp
        )

target_link_libraries(PoissonCAPITests gtest_main)
target_link_libraries(PoissonCAPITests poisson poissonC_API)

set_target_properties(PoissonCAPITests PROPERTIES
        RUNTIME_OUTPUT_DIRECTORY ${POISSON_ROOT_DIR}/build/tests
        )
endif()
