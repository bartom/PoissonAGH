#include "PoissonAGH/Poisson3D.hpp"
#include "gtest/gtest.h"

extern "C"
{
#include "PoissonAGH/Poisson3D_C.h"
}

using namespace testing;
using namespace PoissonAGH;

TEST(Poisson3DAPITestSuite, createUsingSquareStepConstructor)
{
    Poisson3D_Ptr solver = P3D_new_Poisson3D_cubeStep(0, 1, 0.1);

    EXPECT_FALSE(P3D_isSolved(solver));
    EXPECT_EQ(0, P3D_getXmin(solver));
    EXPECT_EQ(1, P3D_getXmax(solver));
    EXPECT_EQ(0, P3D_getYmin(solver));
    EXPECT_EQ(1, P3D_getYmax(solver));
    EXPECT_EQ(0, P3D_getZmin(solver));
    EXPECT_EQ(1, P3D_getZmax(solver));
    EXPECT_EQ(0.1, P3D_getStep(solver));
    EXPECT_EQ(9u, P3D_getNx(solver));
    EXPECT_EQ(9u, P3D_getNy(solver));
    EXPECT_EQ(9u, P3D_getNz(solver));

    P3D_delete_Poisson3D(solver);
}

TEST(Poisson3DAPITestSuite, createUsingSquareSizeConstructor)
{
    Poisson3D_Ptr solver = P3D_new_Poisson3D_cubeSize(0, 1, 9u);

    EXPECT_FALSE(P3D_isSolved(solver));
    EXPECT_EQ(0, P3D_getXmin(solver));
    EXPECT_EQ(1, P3D_getXmax(solver));
    EXPECT_EQ(0, P3D_getYmin(solver));
    EXPECT_EQ(1, P3D_getYmax(solver));
    EXPECT_EQ(0, P3D_getZmin(solver));
    EXPECT_EQ(1, P3D_getZmax(solver));
    EXPECT_EQ(0.1, P3D_getStep(solver));
    EXPECT_EQ(9u, P3D_getNx(solver));
    EXPECT_EQ(9u, P3D_getNy(solver));
    EXPECT_EQ(9u, P3D_getNz(solver));

    P3D_delete_Poisson3D(solver);
}

TEST(Poisson3DAPITestSuite, createUsingRectangleStepConstructor)
{
    Poisson3D_Ptr solver = P3D_new_Poisson3D_cuboidalStep(0, 1, -1, 2, 4, 6, 0.1);

    EXPECT_FALSE(P3D_isSolved(solver));
    EXPECT_EQ(0, P3D_getXmin(solver));
    EXPECT_EQ(1, P3D_getXmax(solver));
    EXPECT_EQ(-1, P3D_getYmin(solver));
    EXPECT_EQ(2, P3D_getYmax(solver));
    EXPECT_EQ(4, P3D_getZmin(solver));
    EXPECT_EQ(6, P3D_getZmax(solver));
    EXPECT_EQ(0.1, P3D_getStep(solver));
    EXPECT_EQ(9u, P3D_getNx(solver));
    EXPECT_EQ(29u, P3D_getNy(solver));
    EXPECT_EQ(19u, P3D_getNz(solver));

    P3D_delete_Poisson3D(solver);
}

TEST(Poisson3DAPITestSuite, createUsingMatrixConstructor)
{
    size_t rows = 6;
    size_t cols = 5;
    size_t slices = 3;
    double*** matrix = (double***) malloc(rows * sizeof(double**));

    for(size_t i = 0; i < rows; ++i)
    {
        matrix[i] = (double**) malloc(cols * sizeof(double*));

        for(size_t j = 0; j < cols; ++j)
        {
            matrix[i][j] = (double*) malloc(slices * sizeof(double));
            matrix[i][j][0] = i*2+j*3+1;
            matrix[i][j][1] = i*3+j*4+2;
            matrix[i][j][2] = i*4+j*5+3;
        }
    }

    Poisson3D_Ptr solver = P3D_new_Poisson3D_matrix(matrix, rows, cols, slices, 0.1);

    EXPECT_FALSE(P3D_isSolved(solver));
    EXPECT_EQ(0.1, P3D_getStep(solver));
    EXPECT_EQ(rows-2, P3D_getNx(solver));
    EXPECT_EQ(cols-2, P3D_getNy(solver));
    EXPECT_EQ(slices-2, P3D_getNz(solver));

    const double* data = P3D_getRhoMatrix(solver);

    for(size_t i = 0; i < rows; ++i)
    {
        for(size_t j = 0; j < cols; ++j)
        {
            for(size_t k = 0; k < slices; ++k)
            {
                EXPECT_EQ(matrix[i][j][k], P3D_getValueAt(data, cols, slices, i, j, k));
            }
        }
    }

    P3D_delete_Poisson3D(solver);
}

TEST(Poisson3DAPITestSuite, createUsingArrayConstructor)
{
    size_t rows = 6;
    size_t cols = 5;
    size_t slices = 3;

    const size_t length = rows * cols * slices;
    double array[length];
    std::iota(array, array+length, 0);

    Poisson3D_Ptr solver = P3D_new_Poisson3D_array(array, rows, cols, slices, 0.1);

    EXPECT_FALSE(P3D_isSolved(solver));
    EXPECT_EQ(0.1, P3D_getStep(solver));
    EXPECT_EQ(rows-2, P3D_getNx(solver));
    EXPECT_EQ(cols-2, P3D_getNy(solver));
    EXPECT_EQ(slices-2, P3D_getNz(solver));

    const double* data = P3D_getRhoMatrix(solver);

    for(size_t i = 0; i < rows*cols*slices; ++i)
    {
        EXPECT_EQ(data[i], array[i]);
    }

    P3D_delete_Poisson3D(solver);
}

TEST(Poisson3DAPITestSuite, shallSetEpsilonC)
{
    Poisson3D_Ptr solver = P3D_new_Poisson3D_cubeStep(0.5, 1, 0.1);

    P3D_setEpsilonC(solver, 3.1415);
    EXPECT_EQ(3.1415, P3D_getEpsilonC(solver));

    P3D_delete_Poisson3D(solver);
}

TEST(Poisson3DAPITestSuite, shallSetEpsilonMatrix)
{
    Poisson3D_Ptr solver = P3D_new_Poisson3D_cubeSize(1, 2, 3);

    const size_t length = 5*5*5;
    double epsilon[length];
    std::iota(epsilon, epsilon+length, 1);

    P3D_setEpsilonMatrix(solver, epsilon);

    const double* data = P3D_getEpsilonMatrix(solver);

    for(size_t i = 0; i < length; ++i)
    {
        EXPECT_EQ(data[i], i+1);
    }

    P3D_delete_Poisson3D(solver);
}

TEST(Poisson3DAPITestSuite, shallSetBoundaryConditions)
{
    size_t rows = 6;
    size_t cols = 5;
    size_t slices = 3;

    const size_t length = rows * cols * slices;
    double array[length];
    std::iota(array, array+length, 0);

    Poisson3D_Ptr solver = P3D_new_Poisson3D_array(array, rows, cols, slices, 0.1);

    Matrix2D front(cols, slices);
    Matrix2D back(cols, slices);
    Matrix2D up(rows, cols);
    Matrix2D down(rows, cols);
    Matrix2D left(rows, slices);
    Matrix2D right(rows, slices);

    for(size_t i = 0; i < rows; ++i)
    {
        for(size_t j = 0; j < cols; ++j)
        {
            up(i, j) = 100*(i+1) + 10*(j+1) + 3;
            down(i, j) = 100*(i+1) + 10*(j+1) + 1;
            for(size_t k = 0; k < slices; ++k)
            {
                front(j, k) = 100 + (j+1)*10 + (k+1);
                back(j, k) = 600 + (j+1)*10 + (k+1);

                left(i, k) = 100*(i+1) + 10 + (k+1);
                right(i, k) = 100*(i+1) + 50 + (k+1);
            }
        }
    }

    P3D_setCondition(solver, front.getData(), cols, slices, DIRICHLET_FRONT);
    P3D_setCondition(solver, back.getData(), cols, slices, DIRICHLET_BACK);
    P3D_setCondition(solver, up.getData(), rows, cols, DIRICHLET_UP);
    P3D_setCondition(solver, down.getData(), rows, cols, DIRICHLET_DOWN);
    P3D_setCondition(solver, left.getData(), rows, slices, DIRICHLET_LEFT);
    P3D_setCondition(solver, right.getData(), rows, slices, DIRICHLET_RIGHT);

    const double * phi = P3D_getPhiMatrix(solver);

    for(size_t i = 0; i < rows; ++i)
    {
        for(size_t j = 0; j < cols; ++j)
        {
            EXPECT_EQ(up(i, j), P3D_getValueAt(phi, cols, slices, i, j, slices-1));
            EXPECT_EQ(down(i, j), P3D_getValueAt(phi, cols, slices, i, j, 0));

            for(size_t k = 0; k < slices; ++k)
            {
                EXPECT_EQ(front(j, k), P3D_getValueAt(phi, cols, slices, 0, j, k));
                EXPECT_EQ(back(j, k), P3D_getValueAt(phi, cols, slices, rows-1, j, k));

                EXPECT_EQ(left(i, k), P3D_getValueAt(phi, cols, slices, i, 0, k));
                EXPECT_EQ(right(i, k), P3D_getValueAt(phi, cols, slices, i, cols-1, k));
            }
        }
    }

    EXPECT_EQ((int)DIRICHLET_FRONT, P3D_getFrontCondition(solver));
    EXPECT_EQ(DIRICHLET_BACK, P3D_getBackCondition(solver));
    EXPECT_EQ(DIRICHLET_UP, P3D_getUpCondition(solver));
    EXPECT_EQ(DIRICHLET_DOWN, P3D_getDownCondition(solver));
    EXPECT_EQ(DIRICHLET_LEFT, P3D_getLeftCondition(solver));
    EXPECT_EQ(DIRICHLET_RIGHT, P3D_getRightCondition(solver));

    P3D_setCondition(solver, front.getData(), cols, slices, NEUMANN_FRONT);
    P3D_setCondition(solver, back.getData(), cols, slices, NEUMANN_BACK);
    P3D_setCondition(solver, up.getData(), rows, cols, NEUMANN_UP);
    P3D_setCondition(solver, down.getData(), rows, cols, NEUMANN_DOWN);
    P3D_setCondition(solver, left.getData(), rows, slices, NEUMANN_LEFT);
    P3D_setCondition(solver, right.getData(), rows, slices, NEUMANN_RIGHT);

    EXPECT_EQ(NEUMANN_FRONT, P3D_getFrontCondition(solver));
    EXPECT_EQ(NEUMANN_BACK, P3D_getBackCondition(solver));
    EXPECT_EQ(NEUMANN_UP, P3D_getUpCondition(solver));
    EXPECT_EQ(NEUMANN_DOWN, P3D_getDownCondition(solver));
    EXPECT_EQ(NEUMANN_LEFT, P3D_getLeftCondition(solver));
    EXPECT_EQ(NEUMANN_RIGHT, P3D_getRightCondition(solver));

    P3D_delete_Poisson3D(solver);
}

double density(double x, double y, double z)
{
    return 2*( (6*x*x-1)*y*y*(y*y-1)*z*z*(z*z-1) +
               (6*y*y-1)*x*x*(x*x-1)*z*z*(z*z-1) +
               (6*z*z-1)*x*x*(x*x-1)*y*y*(y*y-1)
    );
};

double solution(double x, double y, double z)
{
    return -(x*x-x*x*x*x)*(y*y*y*y-y*y)*(z*z-z*z*z*z);
};

TEST(Poisson3DAPITestSuite, shallSolveProblem)
{
    double xmin = 0;
    double xmax = 1;
    size_t nx = 63;
    double h = (xmax - xmin) / static_cast<double>(nx+1);

    const size_t len = 65*65*65;
    double* rho = new double[len];

    for(size_t i = 0; i < nx+2; ++i)
    {
        for(size_t j = 0; j < nx+2; ++j)
        {
            for(size_t k = 0; k < nx+2; ++k)
            {
                P3D_setValueAt(rho, density(xmin + i*h, xmin + j*h, xmin + k*h), nx+2, nx+2, i, j, k);
            }
        }
    }

    Poisson3D_Ptr solver = P3D_new_Poisson3D_array(rho, nx+2, nx+2, nx+2, h);

    P3D_setSolverSteps(solver, 2);
    P3D_setMaxMultigridCalls(solver, 1);
    P3D_setMultigridType(solver, FULL_MULTIGRID);
    P3D_setEpsilonC(solver, 1);
    P3D_setResidualDelta(solver, 1);
    P3D_solve(solver);

    const double* phi = P3D_getPhiMatrix(solver);

    for(size_t i = 1; i <= nx; ++i)
    {
        for(size_t j = 1; j <= nx; ++j)
        {
            for(size_t k = 1; k <= nx; ++k)
            {
                EXPECT_NEAR(solution(xmin+i*h, xmin+j*h, xmin+k*h), P3D_getValueAt(phi, 65, 65, i, j, k), 1e-4);
            }
        }
    }

    P3D_delete_Poisson3D(solver);

    delete[] rho;
}
