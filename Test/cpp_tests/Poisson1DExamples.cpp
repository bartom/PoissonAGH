#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <memory>
#include "PoissonAGH/Poisson1D.hpp"

namespace PoissonAGH
{

using namespace std;
using namespace testing;

TEST(Poisson1DExampleTests, testSimpleDirichletDirichlet){
    // const function
    // homogenous dirichlet - dirichlet
    // epsilon = 1
    const auto solution = [](double x){ return -(x*(x-1))*0.5; };
    const double tol = 1e-8;

    // 1. create array with data of your choice
    Array1D rho(9, 1.); // size 9, singular density

    // 2. create solver with data and grid step
    // IMPORTANT! solver uses move semantics, so rho will be moved
    Poisson1D<ConditionType1D::DirichletDirichlet> solver(rho, GridStep(0.1));
    // or simply Poisson1DConstEpsDD solver(rho, GridStep(0.1));

    // 3. set epsilon if you don't want it to be set to default
    solver.epsilon = PhysicalConstant(1.);

    // 4. solve the problem
    solver.solve();

    Array1D solutionVec(solver.phi.size());
    fillArrayWith1DFunction(solutionVec, solution, solver.gridStep(), 0.1);

    EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), solutionVec));
}

TEST(Poisson1DExampleTests, testSimpleDirichletDirichletWithBC){
    // const function
    // non-homogenous dirichlet - dirichlet
    // epsilon = 1
    auto solution = [](double x){ return -(x*(x+1))*0.5 - 1; };
    double tol = 1e-9;

    Array1D rho(9, 1.);
    Poisson1DConstEpsDD solver(rho, GridStep(0.1));
    solver.epsilon = PhysicalConstant(1.);
    solver.boundary.left = PhysicalConstant(solution(0));
    solver.boundary.right = PhysicalConstant(solution(1));
    solver.solve();

    Array1D solutionVec(solver.phi.size());
    fillArrayWith1DFunction(solutionVec, solution, solver.gridStep(), 0.1);

    EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), solutionVec));

}

TEST(Poisson1DExampleTests, testSimpleDirichletNeumann){
    // const function
    // homogenous dirichlet - neumann
    // epsilon = 1
    auto solution = [](double x){ return -(x*(x-2))*0.5; };
    double tol = 1e-8;

    Array1D rho(10, 1.);
    Poisson1DConstEpsDN solver(rho, GridStep(0.1));
    solver.epsilon = PhysicalConstant(1.);
    solver.solve();

    Array1D solutionVec(solver.phi.size());
    fillArrayWith1DFunction(solutionVec, solution, solver.gridStep(), 0.1);

    EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), solutionVec));
}

TEST(Poisson1DExampleTests, testSimpleNeumannDirichlet){
    // const function
    // homogenous neumann - dirichlet
    // epsilon = 1
    auto solution = [](double x){ return -(x*x)*0.5 + 0.5; };
    double tol = 1e-8;

    Array1D rho(10, 1.);
    Poisson1DConstEpsND solver(rho, GridStep(0.1));
    solver.epsilon = PhysicalConstant(1.);
    solver.solve();

    Array1D solutionVec(solver.phi.size());
    fillArrayWith1DFunction(solutionVec, solution, solver.gridStep(), 0);

    EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), solutionVec));
}

TEST(Poisson1DExampleTests, testSimpleDirichletDirichletVarEps){
    // const function
    // homogenous dirichlet - dirichlet
    auto solution = [](double x){ return -(x*(x-1))*0.5; };
    double tol = 1e-8;

    Array1D rho(9, 1.);
    Array1D epsilonArr(9, 1);
    Poisson1DVarEpsDD solver(rho, GridStep(0.1));
    solver.epsilon = std::move(epsilonArr);
    solver.solve();

    Array1D solutionVec(solver.phi.size());
    fillArrayWith1DFunction(solutionVec, solution, solver.gridStep(), 0.1);

    EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), solutionVec));

}

TEST(Poisson1DExampleTests, testSimpleDirichletDirichletVarEpsWithBC){
    // const function
    // non-homogenous dirichlet - dirichlet
    auto solution = [](double x){ return -(x*(x+1))*0.5 - 1; };
    double tol = 1e-8;

    Array1D rho(9, 1.);
    Array1D epsilonArr(9, 1);
    Poisson1DVarEpsDD solver(rho, GridStep(0.1));
    solver.epsilon = std::move(epsilonArr);
    solver.boundary.left = PhysicalConstant(solution(0));
    solver.boundary.right = PhysicalConstant(solution(1));
    solver.solve();

    Array1D solutionVec(solver.phi.size());
    fillArrayWith1DFunction(solutionVec, solution, solver.gridStep(), 0.1);

    EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), solutionVec));
}

TEST(Poisson1DExampleTests, testSimpleDirichletNeumannVarEps){
    // const function
    // homogenous dirichlet - neumann
    auto solution = [](double x){ return -(x*(x-2))*0.5; };
    double tol = 1e-8;

    Array1D rho(10, 1.);
    Array1D epsilonArr(10, 1);
    Poisson1DVarEpsDN solver(rho, GridStep(0.1));
    solver.epsilon = std::move(epsilonArr);
    solver.solve();

    Array1D solutionVec(solver.phi.size());
    fillArrayWith1DFunction(solutionVec, solution, solver.gridStep(), 0.1);

    EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), solutionVec));
}

TEST(Poisson1DExampleTests, testSimpleNeumannDirichletVarEps){
    // const function
    // homogenous neumann - dirichlet
    // epsilon = 1
    auto solution = [](double x){ return -(x*x)*0.5 + 0.5; };
    double tol = 1e-8;

    Array1D rho(10, 1.);
    Array1D epsilonArr(10, 1);
    Poisson1DVarEpsND solver(rho, GridStep(0.1));
    solver.epsilon = std::move(epsilonArr);
    solver.solve();

    Array1D solutionVec(solver.phi.size());
    fillArrayWith1DFunction(solutionVec, solution, solver.gridStep(), 0);

    EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), solutionVec));

}

TEST(Poisson1DExampleTests, testDirichletDirichlet){
    // linear function
    // non-homogenous dirichlet - dirichlet
    auto solution = [](double x){ return -x*x*x - 1; };
    auto rho = [](double x){ return 6*x; };
    double tol = 1e-6;

    Array1D rhoArr(99);
    GridStep h(0.01);
    fillArrayWith1DFunction(rhoArr, rho, h, h.value());
    Poisson1DConstEpsDD solver(rhoArr, h);
    solver.epsilon = PhysicalConstant(1.);
    solver.boundary.left = PhysicalConstant(solution(0));
    solver.boundary.right = PhysicalConstant(solution(1));
    solver.solve();

    Array1D solutionVec(solver.phi.size());
    fillArrayWith1DFunction(solutionVec, solution, solver.gridStep(), h.value());

    EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), solutionVec));
}

TEST(Poisson1DExampleTests, testDirichletDirichletVarEps){
    // linear function
    // non-homogenous dirichlet - dirichlet
    auto solution = [](double x){ return -2*x; };
    auto rho = [](double x){ return 4*x; };
    auto epsilon = [](double x){ return x*x; };
    double tol = 1e-3;

    Array1D rhoArr(999);
    GridStep h(0.001);
    fillArrayWith1DFunction(rhoArr, rho, h, h.value());
    Poisson1DVarEpsDD solver(rhoArr, h);

    Array1D epsilonArr(999);
    fillArrayWith1DFunction(epsilonArr, epsilon, h, h.value());
    solver.epsilon = std::move(epsilonArr);

    solver.boundary.right = PhysicalConstant(solution(1));
    solver.solve();

    Array1D solutionVec(solver.phi.size());
    fillArrayWith1DFunction(solutionVec, solution, solver.gridStep(), h.value());

    EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), solutionVec));

}

TEST(Poisson1DExampleTests, testDirichletDirichletVarEps2)
{
    // non-homogenous dirichlet - dirichlet
    Array1D solutionVec =
{ 0.0333399, 0.0666907, 0.100064, 0.133469, 0.166919, 0.200424,
  0.233994, 0.267642, 0.301379, 0.335215, 0.369162, 0.403231, 0.437434,
  0.471781, 0.506284, 0.540954, 0.575802, 0.610837, 0.64607, 0.681509,
  0.717161, 0.753033, 0.78913, 0.825452, 0.861999, 0.898765, 0.93574,
  0.97291, 1.01025, 1.04774, 1.08533, 1.12299, 1.16064, 1.19823,
  1.23568, 1.27289, 1.30976, 1.34617, 1.382, 1.41711, 1.45135, 1.48458,
  1.51664, 1.54737, 1.57661, 1.60421, 1.63004, 1.65395, 1.67584,
  1.69561, 1.71317, 1.72847, 1.74147, 1.75215, 1.76054, 1.76665,
  1.77055, 1.7723, 1.77199, 1.76972, 1.7656, 1.75975, 1.75229, 1.74334,
  1.73302, 1.72146, 1.70876, 1.69502, 1.68036, 1.66485, 1.64857,
  1.63159, 1.61398, 1.59579, 1.57707, 1.55784, 1.53816, 1.51804,
  1.4975, 1.47658, 1.45528, 1.43362, 1.41162, 1.38929, 1.36664,
  1.34368, 1.32044, 1.29693, 1.27315, 1.24914, 1.2249, 1.20045,
  1.17581, 1.15101, 1.12607, 1.101, 1.07584, 1.0506, 1.02531 } ;

    auto rho = [](double x){ return 50.*exp(-50.*(x-0.5)*(x-0.5)); };
    auto epsilon = [](double x){ return 2.+cos(M_PI*x); };
    double tol = 1e-3;

    Array1D rhoArr(99);
    GridStep h(0.01);
    fillArrayWith1DFunction(rhoArr, rho, h, h.value());
    Poisson1DVarEpsDD solver(rhoArr, h);

    Array1D epsilonArr(99);
    fillArrayWith1DFunction(epsilonArr, epsilon, h, h.value());
    solver.epsilon = std::move(epsilonArr);
    solver.boundary.left = PhysicalConstant(0.);
    solver.boundary.right = PhysicalConstant(1.);
    solver.solve();

    EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), solutionVec));
}

double calcError(GridStep h, const Array1D& numericSolution, const Array1D& exactSolution)
{
    Array1D result;
    std::transform(numericSolution.cbegin(), numericSolution.cend(), exactSolution.cbegin(),
            std::back_inserter(result), [&](double l, double r)
                   {
                        return sqr(l - r);
                   });
    return sqrt(h.value() * std::accumulate(result.cbegin(), result.cend(), 0.));
}

struct Poisson1DExampleTestSuite : Test
{
    static constexpr double xLeft = 0;
    static constexpr double xRight = 1;

    template<typename Solver>
    static Solver prepareSolver(Array1D& rhoArr, GridStep h, Function1D rho, Function1D phi)
    {
        fillArrayWith1DFunction(rhoArr, rho, h, h.value());
        Solver solver(rhoArr, h);
        solver.boundary.left = PhysicalConstant(phi(xLeft));
        solver.boundary.right = PhysicalConstant(phi(xRight));
        return solver;
    }

    template<typename Solver>
    static void checkResult(const Solver& solver, GridStep h, Function1D phi, double tol)
    {
        Array1D phiVec(solver.phi.size());
        fillArrayWith1DFunction(phiVec, phi, solver.gridStep(), h.value());

        EXPECT_THAT(solver.phi, Pointwise(FloatNear(tol), phiVec));

        cout << "N = " << solver.phi.size() << ", error: ";
        cout << calcError(h, solver.phi, phiVec) << endl;
    }
};

struct Poisson1DExampleDirichletDirichletConstEps : Poisson1DExampleTestSuite
{
    static double rho(double) { return -1; }
    static double phi(double x) { return 0.5*x * (x + 1) + 1; }
    static constexpr PhysicalConstant eps = PhysicalConstant(1.);
    static constexpr double tol = 1e-13;

    static void executeTest(Array1D& rhoArr, GridStep h)
    {
        Poisson1DConstEpsDD solver = prepareSolver<Poisson1DConstEpsDD>(rhoArr, h, rho, phi);
        solver.epsilon = eps;
        solver.solve();

        checkResult(solver, h, phi, tol);
    }
};

TEST_F(Poisson1DExampleDirichletDirichletConstEps, N9)
{
    Array1D rhoArr(9);
    GridStep h(0.1);

    executeTest(rhoArr, h);
}

TEST_F(Poisson1DExampleDirichletDirichletConstEps, N99)
{
    Array1D rhoArr(99);
    GridStep h(0.01);

    executeTest(rhoArr, h);
}

TEST_F(Poisson1DExampleDirichletDirichletConstEps, N999)
{
    Array1D rhoArr(999);
    GridStep h(0.001);

    executeTest(rhoArr, h);
}

struct Poisson1DExampleDirichletDirichletVarEps : Poisson1DExampleTestSuite
{
    static double rho(double x) { return -4*x; }
    static double phi(double x) { return 2*x; }
    static double eps(double x) { return sqr(x); }

    static constexpr double tol = 1e-3;

    static void executeTest(Array1D& rhoArr, GridStep h)
    {
        Poisson1DVarEpsDD solver = prepareSolver<Poisson1DVarEpsDD>(rhoArr, h, rho, phi);

        Array1D epsArr(rhoArr.size());
        fillArrayWith1DFunction(epsArr, eps, solver.gridStep(), h.value());
        solver.epsilon = std::move(epsArr);

        solver.solve();

        checkResult(solver, h, phi, tol);
    }

};

TEST_F(Poisson1DExampleDirichletDirichletVarEps, N999)
{
    Array1D rhoArr(999);
    GridStep h(0.001);

    executeTest(rhoArr, h);
}

TEST_F(Poisson1DExampleDirichletDirichletVarEps, N1999)
{
    Array1D rhoArr(1999);
    GridStep h(0.0005);

    executeTest(rhoArr, h);
}

TEST_F(Poisson1DExampleDirichletDirichletVarEps, N4999)
{
    Array1D rhoArr(4999);
    GridStep h(0.0002);

    executeTest(rhoArr, h);
}

TEST_F(Poisson1DExampleDirichletDirichletVarEps, N9999)
{
    Array1D rhoArr(9999);
    GridStep h(0.0001);

    executeTest(rhoArr, h);
}

constexpr double threePointDerivative(Function1D phi, double x, double h)
{
    return (phi(x + h) - phi(x - h)) / (2*h);
}

struct Poisson1DExampleDirichletNeumannVarEps : Poisson1DExampleTestSuite
{
    static double rho(double x) { return 4*(x+1)*std::sin(2*x) - 2*std::cos(2*x); }
    static double phi(double x) { return std::sin(2*x); }
    static double eps(double x) { return x+1; }

    static constexpr double tol = 1e-3;

    static void executeTest(Array1D& rhoArr, GridStep h)
    {
        Poisson1DVarEpsDN solver = prepareSolver<Poisson1DVarEpsDN>(rhoArr, h, rho, phi);

        Array1D epsArr(rhoArr.size());
        fillArrayWith1DFunction(epsArr, eps, solver.gridStep(), h.value());
        solver.epsilon = std::move(epsArr);
        solver.boundary.right = PhysicalConstant(threePointDerivative(phi, xRight, h.value()));

        solver.solve();

        checkResult(solver, h, phi, tol);
    }

};

TEST_F(Poisson1DExampleDirichletNeumannVarEps, N1000)
{
    Array1D rhoArr(1000);
    GridStep h(0.001);

    executeTest(rhoArr, h);
}

TEST_F(Poisson1DExampleDirichletNeumannVarEps, N2000)
{
    Array1D rhoArr(2000);
    GridStep h(0.0005);

    executeTest(rhoArr, h);
}

TEST_F(Poisson1DExampleDirichletNeumannVarEps, N5000)
{
    Array1D rhoArr(5000);
    GridStep h(0.0002);

    executeTest(rhoArr, h);
}

TEST_F(Poisson1DExampleDirichletNeumannVarEps, N10000)
{
    Array1D rhoArr(10000);
    GridStep h(0.0001);

    executeTest(rhoArr, h);
}

struct Poisson1DExamplePeriodicVarEps : Poisson1DExampleTestSuite
{
    static double rho(double x) { return (x+1)*std::cos(x) + std::sin(x); }
    static double phi(double x) { return std::cos(x); }
    static double eps(double x) { return x+1; }

    static constexpr double tol = 1e-1;

    static void executeTest(Array1D& rhoArr, GridStep h)
    {
        Poisson1DVarEpsP solver = prepareSolver<Poisson1DVarEpsP>(rhoArr, h, rho, phi);

        Array1D epsArr(rhoArr.size());
        fillArrayWith1DFunction(epsArr, eps, solver.gridStep(), h.value());
        solver.epsilon = std::move(epsArr);

        solver.solve();

        checkResult(solver, h, phi, tol);
    }

};

TEST_F(Poisson1DExamplePeriodicVarEps, N1000)
{
    // x > 0 && x < 2*M_PI
    Array1D rhoArr(1000);
    GridStep h(0.006283185);

    executeTest(rhoArr, h);
}

TEST_F(Poisson1DExamplePeriodicVarEps, N10000)
{
    // x > 0 && x < 2*M_PI
    Array1D rhoArr(10000);
    GridStep h(0.0006283185);

    executeTest(rhoArr, h);
}

}
