#include "gtest/gtest.h"
#include "PoissonAGH/Utils/PTools.hpp"
#include "PoissonAGH/Utils/PMatrix.hpp"
#include "PoissonAGH/Solver/PSolver1D.hpp"

using namespace PoissonAGH;

TEST(PtoolsTestSuite, testThomasAlgorithm1)
{
    double eps = 1e-3;
    size_t N = 4;
    Sparse eqMatrix;
    eqMatrix.DL = Array1D(N, -1.);
    eqMatrix.D = Array1D(N, 2.04);
    eqMatrix.DU = Array1D(N, -1.);
    eqMatrix.DL[0] = eqMatrix.DU[N-1] = 0.;
    Array1D f(N);
    f[0] = 40.8;
    f[1] = 0.8;
    f[2] = 0.8;
    f[3] = 200.8;

    Array1D solution(N);
    solution[0] = 65.97;
    solution[1] = 93.778;
    solution[2] = 124.538;
    solution[3] = 159.480;

    Solver::Detail::Algorithm1D::thomasAlgorithm(eqMatrix, f);

    for(auto i = 0u; i < N; ++i)
    {
        EXPECT_NEAR(f[i], solution[i], eps);
    }
}

TEST(PtoolsTestSuite, testShermanMorrison1)
{
    double eps = 0.05;
    size_t N = 4;
    Sparse eqMatrix;
    eqMatrix.DL = Array1D(N, -1.);
    eqMatrix.D = Array1D(N, 2.04);
    eqMatrix.DU = Array1D(N, -1.);
    Array1D f(N);

    f[0] = 40.8;
    f[1] = 0.8;
    f[2] = 0.8;
    f[3] = 200.8;

    Array1D solution(N);
    solution[0] = 1519.9;
    solution[1] = 1480.9;
    solution[2] = 1500.3;
    solution[3] = 1578.9;

    Solver::Detail::Algorithm1D::shermanMorrison(eqMatrix, f);

    for(auto i = 0u; i < N; ++i)
    {
        EXPECT_NEAR(f[i], solution[i], eps);
    }
}

TEST(PtoolsTestSuite, testConstructionOfMatrix)
{
    Matrix2D a(5);
    Matrix2D b(6, 8);

    ASSERT_EQ(5u, a.cols());
    ASSERT_EQ(5u, a.rows());
    ASSERT_EQ(6u, b.rows());
    ASSERT_EQ(8u, b.cols());

    ASSERT_EQ(a(0, 0), 0);
    ASSERT_EQ(b(0, 0), 0);

    a = b;
    a(0, 0) = 3;

    ASSERT_EQ(6u, a.rows());
    ASSERT_EQ(8u, a.cols());

    ASSERT_EQ(a(0, 0), 3);
}

TEST(PtoolsTestSuite, testConstructionFromStaticArray)
{
    double arr[3][2] = {{9,8}, {7,6}, {5,4}};
    Matrix2D a(*arr, 3, 2);

    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 2; ++j)
        {
            ASSERT_EQ(arr[i][j], a(i, j));
        }
    }
}

TEST(PtoolsTestSuite, testConstructionFromDynamicArray)
{
    double** arr = (double**) malloc(3* sizeof(double*));
    for(int i = 0; i < 3; ++i)
    {
        arr[i] = (double*) malloc(2 * sizeof(double));
        arr[i][0] = i*4;
        arr[i][1] = i*5;
    }
    Matrix2D a(arr, 3, 2);

    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 2; ++j)
        {
            ASSERT_EQ(arr[i][j], a(i, j));
        }
    }
}

TEST(PtoolsTestSuite, testMatrixFill)
{
    Matrix2D a(4, 5);
    for(auto i = 0; i < 4; ++i)
    {
        for(auto j = 0; j < 5; ++j)
        {
            ASSERT_EQ(a(i, j), 0);
        }
    }

    a.fill(46.9);
    for(auto i = 0; i < 4; ++i)
    {
        for(auto j = 0; j < 5; ++j)
        {
            ASSERT_EQ(a(i, j), 46.9);
        }
    }
}

TEST(PtoolsTestSuite, testIf0MatrixSizeWontFail)
{
    Matrix2D a(0);
    ASSERT_EQ(0u, a.rows());
    ASSERT_EQ(0u, a.cols());
}

