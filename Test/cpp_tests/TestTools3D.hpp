#ifndef POISSONAGH_TESTTOOLS3D_HPP
#define POISSONAGH_TESTTOOLS3D_HPP
#include "gtest/gtest.h"
#include "PoissonAGH/Solver/SolverStrategy3D.hpp"
#include "PoissonAGH/Poisson3D.hpp"

namespace PoissonAGH
{
using namespace testing;
using namespace std;

template<typename FunctionSet3D>
void applyConditionsFront(Matrix3D& matrix, ConditionType3D condition, double xmin, double ymin, double zmin, double h)
{
    for(size_t j = 0; j < matrix.cols(); ++j)
    {
        for(size_t k = 0; k < matrix.slices(); ++k)
        {
            condition == ConditionType3D::DirichletFront ?
                    matrix(0, j, k) = FunctionSet3D::solution(xmin, ymin + j*h, zmin + k*h):
                    matrix(0, j, k) = FunctionSet3D::dervX(xmin+h, ymin + j*h, zmin + k*h);
        }
    }
}

template<typename FunctionSet3D>
void applyConditionsBack(Matrix3D& matrix, ConditionType3D condition, double xmax, double ymin, double zmin, double h)
{
    for(size_t j = 0; j < matrix.cols(); ++j)
    {
        for(size_t k = 0; k < matrix.slices(); ++k)
        {
            condition == ConditionType3D::DirichletBack ?
                    matrix(matrix.rows()-1, j, k) = FunctionSet3D::solution(xmax, ymin + j*h, zmin + k*h) :
                    matrix(matrix.rows()-1, j, k) = FunctionSet3D::dervX(xmax-h, ymin + j*h, zmin + k*h);
        }
    }
}

template<typename FunctionSet3D>
void applyConditionsDown(Matrix3D& matrix, ConditionType3D condition, double xmin, double ymin, double zmin, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        for(size_t j = 0; j < matrix.cols(); ++j)
        {
            condition == ConditionType3D::DirichletDown ?
                    matrix(i, j, 0) = FunctionSet3D::solution(xmin + i*h, ymin + j*h, zmin) :
                    matrix(i, j, 0) = FunctionSet3D::dervZ(xmin + i*h, ymin + j*h, zmin+h);
        }
    }
}

template<typename FunctionSet3D>
void applyConditionsUp(Matrix3D& matrix, ConditionType3D condition, double xmin, double ymin, double zmax, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        for(size_t j = 0; j < matrix.cols(); ++j)
        {
            condition == ConditionType3D::DirichletUp ?
                    matrix(i, j, matrix.slices()-1) = FunctionSet3D::solution(xmin + i*h, ymin + j*h, zmax) :
                    matrix(i, j, matrix.slices()-1) = FunctionSet3D::dervZ(xmin + i*h, ymin + j*h, zmax-h);
        }
    }
}

template<typename FunctionSet3D>
void applyConditionsLeft(Matrix3D& matrix, ConditionType3D condition, double xmin, double ymin, double zmin, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        for(size_t k = 0; k < matrix.slices(); ++k)
        {
            condition == ConditionType3D::DirichletLeft ?
                    matrix(i, 0, k) = FunctionSet3D::solution(xmin + i*h, ymin, zmin + k*h) :
                    matrix(i, 0, k) = FunctionSet3D::dervY(xmin + i*h, ymin+h, zmin + k*h);
        }
    }
}

template<typename FunctionSet3D>
void applyConditionsRight(Matrix3D& matrix, ConditionType3D condition, double xmin, double ymax, double zmin, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        for(size_t k = 0; k < matrix.slices(); ++k)
        {
            condition == ConditionType3D::DirichletRight ?
                    matrix(i, matrix.cols()-1, k) = FunctionSet3D::solution(xmin + i*h, ymax, zmin + k*h) :
                    matrix(i, matrix.cols()-1, k) = FunctionSet3D::dervY(xmin + i*h, ymax-h, zmin + k*h);
        }
    }
}

template<typename FunctionSet3D>
void checkResult(const Matrix3D& phi, double xmin, double ymin, double zmin, double h, double error)
{
    double absError = 0;
    for(size_t i = 1; i < phi.rows()-1; ++i)
    {
        for(size_t j = 1; j < phi.cols()-1; ++j)
        {
            for(size_t k = 1; k < phi.slices()-1; ++k)
            {
                EXPECT_NEAR(phi(i, j, k), FunctionSet3D::solution(xmin + i*h, ymin + j*h, zmin + k*h), error);
                absError += std::fabs(phi(i, j, k) - FunctionSet3D::solution(xmin + i*h, ymin + j*h, zmin + k*h));
            }
        }
    }
    cout << "error: " << absError << endl;
}

template<typename FunctionSet3D>
void applyConditionsFront(Matrix2D& matrix, ConditionType3D condition, double xmin, double ymin, double zmin, double h)
{
    for(size_t j = 0; j < matrix.rows(); ++j)
    {
        for(size_t k = 0; k < matrix.cols(); ++k)
        {
            condition == ConditionType3D::DirichletFront ?
                    matrix(j, k) = FunctionSet3D::solution(xmin, ymin + j*h, zmin + k*h) :
                    matrix(j, k) = FunctionSet3D::dervX(xmin+h, ymin + j*h, zmin + k*h);
        }
    }
}

template<typename FunctionSet3D>
void applyConditionsBack(Matrix2D& matrix, ConditionType3D condition, double xmax, double ymin, double zmin, double h)
{
    for(size_t j = 0; j < matrix.rows(); ++j)
    {
        for(size_t k = 0; k < matrix.cols(); ++k)
        {
            condition == ConditionType3D::DirichletBack ?
                    matrix(j, k) = FunctionSet3D::solution(xmax, ymin + j*h, zmin + k*h) :
                    matrix(j, k) = FunctionSet3D::dervX(xmax-h, ymin + j*h, zmin + k*h);
        }
    }
}

template<typename FunctionSet3D>
void applyConditionsDown(Matrix2D& matrix, ConditionType3D condition, double xmin, double ymin, double zmin, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        for(size_t j = 0; j < matrix.cols(); ++j)
        {
            condition == ConditionType3D::DirichletDown ?
                    matrix(i, j) = FunctionSet3D::solution(xmin + i*h, ymin + j*h, zmin) :
                    matrix(i, j) = FunctionSet3D::dervZ(xmin + i*h, ymin + j*h, zmin+h);
        }
    }
}

template<typename FunctionSet3D>
void applyConditionsUp(Matrix2D& matrix, ConditionType3D condition, double xmin, double ymin, double zmax, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        for(size_t j = 0; j < matrix.cols(); ++j)
        {
            condition == ConditionType3D::DirichletUp ?
                    matrix(i, j) = FunctionSet3D::solution(xmin + i*h, ymin + j*h, zmax) :
                    matrix(i, j) = FunctionSet3D::dervZ(xmin + i*h, xmin + j*h, zmax-h);
        }
    }
}

template<typename FunctionSet3D>
void applyConditionsLeft(Matrix2D& matrix, ConditionType3D condition, double xmin, double ymin, double zmin, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        for(size_t k = 0; k < matrix.cols(); ++k)
        {
            condition == ConditionType3D::DirichletLeft ?
                    matrix(i, k) = FunctionSet3D::solution(xmin + i*h, ymin, zmin + k*h) :
                    matrix(i, k) = FunctionSet3D::dervY(xmin + i*h, ymin+h, zmin + k*h);
        }
    }
}

template<typename FunctionSet3D>
void applyConditionsRight(Matrix2D& matrix, ConditionType3D condition, double xmin, double ymax, double zmin, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        for(size_t k = 0; k < matrix.cols(); ++k)
        {
            condition == ConditionType3D::DirichletRight ?
                    matrix(i, k) = FunctionSet3D::solution(xmin + i*h, ymax, zmin + k*h) :
                    matrix(i, k) = FunctionSet3D::dervY(xmin + i*h, ymax-h, zmin + k*h);
        }
    }
}

template<typename FunctionSet3D>
void createBoundaryAndApplyCondition(Poisson3D& problem, ConditionType3D condition, size_t sizeX, size_t sizeY,
                                     double xmin, double ymin, double zmin, double xmax, double ymax, double zmax,
                                     double h)
{
    Matrix2D boundary(sizeX + 2, sizeY + 2);

    switch(condition)
    {
        case ConditionType3D::DirichletFront: //[fallthrough]
        case ConditionType3D::NeumannFront:
        {
            applyConditionsFront<FunctionSet3D>(boundary, condition, xmin, ymin, zmin, h);
            break;
        }
        case ConditionType3D::DirichletBack: //[fallthrough]
        case ConditionType3D::NeumannBack:
        {
            applyConditionsBack<FunctionSet3D>(boundary, condition, xmax, ymin, zmin, h);
            break;
        }
        case ConditionType3D::DirichletUp: //[fallthrough]
        case ConditionType3D::NeumannUp:
        {
            applyConditionsUp<FunctionSet3D>(boundary, condition, xmin, ymin, zmax, h);
            break;
        }
        case ConditionType3D::DirichletDown: //[fallthrough]
        case ConditionType3D::NeumannDown:
        {
            applyConditionsDown<FunctionSet3D>(boundary, condition, xmin, ymin, zmin, h);
            break;
        }
        case ConditionType3D::DirichletLeft: //[fallthrough]
        case ConditionType3D::NeumannLeft:
        {
            applyConditionsLeft<FunctionSet3D>(boundary, condition, xmin, ymin, zmin, h);
            break;
        }
        case ConditionType3D::DirichletRight: //[fallthrough]
        case ConditionType3D::NeumannRight:
        {
            applyConditionsRight<FunctionSet3D>(boundary, condition, xmin, ymax, zmin, h);
            break;
        }
    }

    problem.setCondition(boundary, condition);
}

inline void fillMatrix3DWithFunction(Matrix3D& matrix, Function3D func, double xmin, double ymin, double zmin, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        for(size_t j = 0; j < matrix.cols(); ++j)
        {
            for(size_t k = 0; k < matrix.slices(); ++k)
            {
                matrix(i, j, k) = func(xmin + i*h, ymin + j*h, zmin + k*h);
            }
        }
    }
}

inline void checkResidual(Matrix3D& rho, double residuum, double expected)
{
    auto residual = std::accumulate(rho.getData(), rho.getData()+rho.rows()*rho.cols()*rho.slices(), 0.);
    cout << residual << endl;
    cout << residuum << endl;
    EXPECT_NEAR(residuum, expected, fabs(expected) / 100);
}

inline void assertResultNear(const Poisson3D& solver, Function3D solution, double xmin, double ymin, double zmin,
                             double tolerance)
{
    const auto h = solver.gridStep();
    for(size_t i = 1; i <= solver.nX(); ++i)
    {
        for(size_t j = 1; j <= solver.nY(); ++j)
        {
            for(size_t k = 1; k <= solver.nZ(); ++k)
            {
                ASSERT_NEAR(solver.phi(i, j, k), solution(xmin+i*h, ymin+j*h, zmin+k*h), tolerance);
            }
        }
    }
}

inline double calculate3DL2Norm(const Poisson3D& solver, Function3D solution, double xmin, double ymin, double zmin)
{
    const auto h = solver.gridStep();
    double norm = 0.;

    for(size_t i = 1; i <= solver.nX(); ++i)
    {
        for(size_t j = 1; j <= solver.nY(); ++j)
        {
            for(size_t k = 1; k <= solver.nZ(); ++k)
            {
                norm += sqr( solver.phi(i, j, k) - solution(xmin + i*h, ymin + j*h, zmin + k*h) );
            }
        }
    }

    return std::sqrt(pow(h, 3) * norm);
}

inline void printResult(const Poisson3D& problem, Function3D solution, MultigridCycle cycle,
                        double xmin, double ymin, double zmin, Milis time, bool perf = false)
{
    cout << "nx = " << problem.nX() << " ny = " << problem.nY() << " nz = " << problem.nZ() << ";";
    if(perf)
    {
        cout << "Gauss-Seidel perf test;";
    }
    else
    {
        cout << "cycle: " << cycle << ";";
    }
    cout << "|e| = "<< calculate3DL2Norm(problem, solution, xmin, ymin, zmin) << ";"
         << "steps total = " << problem.solverStepsTotal.value() << ";"
         << "duration[ms] = " << time.count() << endl;
}

inline void checkAndPrintResult(const Poisson3D& problem, Function3D solution, MultigridCycle cycle,
                                double xmin, double ymin, double zmin, double tol, Milis time)
{
    assertResultNear(problem, solution, xmin, ymin, zmin, tol);
    printResult(problem, solution, cycle, xmin, ymin, zmin, time);
}

inline Milis solveWithTimer(Poisson3D& problem)
{
    auto before = Clock::now();
    problem.solve();
    auto after = Clock::now();
    return std::chrono::duration_cast<Milis>(after - before);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////// TEST FUNCTION CLASSES /////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class FunctionSet3DConstEps_1
{
public:
    static double density(double x, double y, double z)
    {
        return 2*sin(x)*sin(2*y)*sin(3*z);
    }

    static double solution(double x, double y, double z)
    {
        return sin(x)*sin(2*y)*sin(3*z);
    }

    static double dervX(double x, double y, double z)
    {
        return cos(x)*sin(2*y)*sin(3*z);
    }

    static double dervY(double x, double y, double z)
    {
        return 2*cos(2*y)*sin(x)*sin(3*z);
    }

    static double dervZ(double x, double y, double z)
    {
        return 3*cos(3*z)*sin(x)*sin(2*y);
    }

    static double eps(double = 0, double = 0, double = 0)
    {
        return 1./7.;
    }

};

class FunctionSet3DConstEps_2
{
public:
    static double density(double x, double y, double z)
    {
        return 6*exp(-sqr(x)-sqr(y)-sqr(z)) -
               4*exp(-sqr(x)-sqr(y)-sqr(z))*sqr(x) -
               4*exp(-sqr(x)-sqr(y)-sqr(z))*sqr(y) -
               4*exp(-sqr(x)-sqr(y)-sqr(z))*sqr(z);
    }

    static double solution(double x, double y, double z)
    {
        return exp(-sqr(x)-sqr(y)-sqr(z));
    }

    static double dervX(double x, double y, double z)
    {
        return -2*exp(-sqr(x)-sqr(y)-sqr(z))*x;
    }

    static double dervY(double x, double y, double z)
    {
        return -2*exp(-sqr(x)-sqr(y)-sqr(z))*y;
    }

    static double dervZ(double x, double y, double z)
    {
        return -2*exp(-sqr(x)-sqr(y)-sqr(z))*z;
    }

    static double eps(double = 0, double = 0, double = 0)
    {
        return 1.;
    }

};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class FunctionSet3DVarEps_1
{
public:
    static double density(double x, double y, double z)
    {
        return -3*cos(3*z)*sin(x)*sin(2*y) - 2*cos(2*y)*sin(x)*sin(3*z)
               - cos(x)*sin(2*y)*sin(3*z) + 14*(1+x+y+z)*sin(x)*sin(2*y)*sin(3*z);
    }

    static double solution(double x, double y, double z)
    {
        return sin(x)*sin(2*y)*sin(3*z);
    }

    static double dervX(double x, double y, double z)
    {
        return cos(x)*sin(2*y)*sin(3*z);
    }

    static double dervY(double x, double y, double z)
    {
        return 2*cos(2*y)*sin(x)*sin(3*z);
    }

    static double dervZ(double x, double y, double z)
    {
        return 3*cos(3*z)*sin(x)*sin(2*y);
    }

    static double eps(double x, double y, double z)
    {
        return 1 + x + y + z;
    }

};

class FunctionSet3DVarEps_2
{
public:
    static double density(double x, double y, double z)
    {
        return exp(-sqr(x)*(1+pow(x, 4))-pow(y, 6)-pow(z, 6))*( 2*x*(2+3*pow(x, 4))*cos(x)
        + (3-4*(sqr(x)+3*pow(x, 6)) + 6*pow(y, 4)*(5 - 6*pow(y, 6)) + 6*pow(z, 4)*(5 - 6*pow(z, 6)))*sin(x) );
    }

    static double solution(double x, double y, double z)
    {
        return sin(x)*exp(-sqr(x))*exp(-pow(y, 6)-pow(z, 6));
    }

    static double dervX(double x, double y, double z)
    {
        return exp(-sqr(x)-pow(y, 6)-pow(z, 6))*(cos(x) - 2*x*sin(x));
    }

    static double dervY(double x, double y, double z)
    {
        return -6*exp(-sqr(x)-pow(y, 6)-pow(z, 6))*pow(y, 5)*sin(x);
    }

    static double dervZ(double x, double y, double z)
    {
        return -6*exp(-sqr(x)-pow(y, 6)-pow(z, 6))*pow(z, 5)*sin(x);
    }

    static double eps(double x, double y, double z)
    {
        return exp(-pow(x, 6));
    }

};

class FunctionSet3DVarEps_3
{
public:
    static double density(double x, double y, double z)
    {
        return -4*(x-1)*x*(y-1)*sqr(y)*z*(2*z-1) -
               2*(x-1)*x*(y-1)*sqr(y)*(2*sqr(z)+1) -
               2*(y-1)*sqr(y)*(z-1)*z*(2*sqr(z)+1) -
               2*(x-1)*x*(3*y-1)*(z-1)*z*(2*sqr(z)+1);
    }

    static double solution(double x, double y, double z)
    {
        return x*(x-1)*sqr(y)*(y-1)*z*(z-1);
    }

    static double dervX(double x, double y, double z)
    {
        return (2*x-1)*(y-1)*sqr(y)*(z-1)*z;
    }

    static double dervY(double x, double y, double z)
    {
        return (x-1)*x*y*(3*y-2)*(z-1)*z;
    }

    static double dervZ(double x, double y, double z)
    {
        return (x-1)*x*(y-1)*sqr(y)*(2*z-1);
    }

    static double eps(double x, double y, double z)
    {
        return 1 + 2*sqr(z);
    }

};


}

#endif //POISSONAGH_TESTTOOLS3D_HPP
