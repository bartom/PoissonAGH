#include "gtest/gtest.h"
#include "PoissonAGH/Poisson2D.hpp"
#include "TestTools2D.hpp"

namespace PoissonAGH
{

using namespace std;
using namespace testing;

struct TestParams
{
    double xmin;
    double xmax;
    size_t nx;
    size_t steps;
    size_t multigridCalls;
    MultigridCycle methodType;
    double error;
};

class FunctionSet3DConstEps_0
{
public:
    static double density(double x, double y)
    {
        return 2*( (1-6*x*x)*y*y*(1-y*y) + (1-6*y*y)*x*x*(1-x*x) );
    };

    static double solution(double x, double y)
    {
        return (x*x-x*x*x*x) * (y*y*y*y - y*y);
    };

};

class Poisson2DTestSuite1: public TestWithParam<TestParams>
{
};

TEST_P(Poisson2DTestSuite1, shallSolveProblem)
{
    TestParams params = GetParam();
    double xmin = params.xmin;
    double xmax = params.xmax;
    size_t nx = params.nx;
    double h = getH(xmin, xmax, nx);

    Matrix2D rho(nx + 2);

    fillMatrix2DWithFunction(rho, FunctionSet3DConstEps_0::density, xmin, xmin, h);

    Poisson2D solver(rho, GridStep(h));

    solver.solverSteps = Steps(params.steps);
    solver.maxCalls = SolverCalls(params.multigridCalls);
    solver.setMultigridCycle(params.methodType);
    solver.epsilonC = PhysicalConstant(1);

    auto time = solveWithTimer(solver);
    checkAndPrintResult(solver, FunctionSet3DConstEps_0::solution, params.methodType, xmin, xmin, params.error, time);
    cout << endl;
}

INSTANTIATE_TEST_CASE_P(HomogeneusDirichlet,
                        Poisson2DTestSuite1,
                        ::testing::Values(
                                //xmin, xmax, nx, solver steps, mg calls, mg type, max error
                                TestParams{0, 1, 63, 2, 2, MultigridCycle::FullMultigrid, 1e-4},
                                TestParams{0, 1, 63, 2, 5, MultigridCycle::VCycle, 1e-4},
                                TestParams{0, 1, 63, 2, 5, MultigridCycle::WCycle, 1e-4},
                                TestParams{-1, 1, 127, 2, 2, MultigridCycle::FullMultigrid, 1e-4},
                                TestParams{-1, 1, 127, 2, 5, MultigridCycle::VCycle, 1e-4},
                                TestParams{-1, 1, 127, 2, 5, MultigridCycle::WCycle, 1e-4},
                                TestParams{-1, 1, 511, 2, 2, MultigridCycle::FullMultigrid, 1e-4},
                                TestParams{-1, 1, 511, 2, 5, MultigridCycle::VCycle, 1e-4},
                                TestParams{-1, 1, 511, 2, 5, MultigridCycle::WCycle, 1e-4}
                        ));


class Poisson2DTestSuite2: public TestWithParam<TestParams>
{
};

TEST_P(Poisson2DTestSuite2, shallSolveProblem)
{
    TestParams params = GetParam();
    double xmin = params.xmin;
    double xmax = params.xmax;
    size_t nx = params.nx;
    double h = getH(xmin, xmax, nx);

    Array1D westBoundary(params.nx + 2);
    Array1D eastBoundary(params.nx + 2);
    Array1D northBoundary(params.nx + 2);
    Array1D southBoundary(params.nx + 2);

    Matrix2D rho(nx + 2);

    fillMatrix2DWithFunction(rho, FunctionSet2DConstEps_3::density, xmin, xmin, h);
    applyConditionNorth<FunctionSet2DConstEps_3>(northBoundary, ConditionType2D::DirichletNorth, xmax, xmin, h);
    applyConditionSouth<FunctionSet2DConstEps_3>(southBoundary, ConditionType2D::DirichletSouth, xmin, xmin, h);
    applyConditionWest<FunctionSet2DConstEps_3>(westBoundary, ConditionType2D::DirichletWest, xmin, xmin, h);
    applyConditionEast<FunctionSet2DConstEps_3>(eastBoundary, ConditionType2D::DirichletEast, xmin, xmax, h);

    Poisson2D solver(rho, GridStep(h));

    solver.solverSteps = Steps(params.steps);
    solver.maxCalls = SolverCalls(params.multigridCalls);
    solver.setMultigridCycle(params.methodType);
    solver.epsilonC = PhysicalConstant(1);

    solver.setCondition(westBoundary, ConditionType2D::DirichletWest);
    solver.setCondition(eastBoundary, ConditionType2D::DirichletEast);
    solver.setCondition(northBoundary, ConditionType2D::DirichletNorth);
    solver.setCondition(southBoundary, ConditionType2D::DirichletSouth);

    auto time = solveWithTimer(solver);
    checkAndPrintResult(solver, FunctionSet2DConstEps_3::solution, params.methodType, xmin, xmin, params.error, time);
    cout << endl;
}

INSTANTIATE_TEST_CASE_P(NonHomogeneusDirichlet,
                        Poisson2DTestSuite2,
                        ::testing::Values(
                                //xmin, xmax, nx, solver steps, mg calls, mg type, max error
                                TestParams{-1, 1, 255, 2, 5, MultigridCycle::VCycle, 1e-4},
                                TestParams{-1, 1, 255, 2, 5, MultigridCycle::WCycle, 1e-4},
                                TestParams{-1, 1, 255, 2, 3, MultigridCycle::FullMultigrid, 1e-4},
                                TestParams{-2, 2, 511, 2, 5, MultigridCycle::VCycle, 1e-4},
                                TestParams{-2, 2, 511, 2, 5, MultigridCycle::WCycle, 1e-4},
                                TestParams{-2, 2, 511, 2, 2, MultigridCycle::FullMultigrid, 1e-4},
                                TestParams{-2, 2, 1023, 2, 5, MultigridCycle::VCycle, 1e-5},
                                TestParams{-2, 2, 1023, 2, 5, MultigridCycle::WCycle, 1e-5},
                                TestParams{-2, 2, 1023, 2, 3, MultigridCycle::FullMultigrid, 1e-5}
                        ));


class FunctionSet3
{
public:
    static double density(double x, double y)
    {
        return 5*sin(x)*sin(2*y);
    }

    static double solution(double x, double y)
    {
        return sin(x)*sin(2*y);
    }

    static double derivative(double x, double y)
    {
        return cos(x)*sin(2*y) + 2*cos(2*y)*sin(x);
    }

};

class Poisson2DTestSuite3: public FunctionSet2DConstEps_1, public TestWithParam<TestParams>
{
};

TEST_P(Poisson2DTestSuite3, shallSolveProblem)
{
    TestParams params = GetParam();
    double xmin = params.xmin;
    double xmax = params.xmax;
    size_t nx = params.nx;
    double h = getH(xmin, xmax, nx);

    Array1D westBoundary(params.nx + 2);
    Array1D eastBoundary(params.nx + 2);
    Array1D northBoundary(params.nx + 2);
    Array1D southBoundary(params.nx + 2);

    Matrix2D rho(nx + 2);

    fillMatrix2DWithFunction(rho, FunctionSet2DConstEps_1::density, xmin, xmin, h);
    applyConditionNorth<FunctionSet2DConstEps_1>(northBoundary, ConditionType2D::DirichletNorth, xmax, xmin, h);
    applyConditionSouth<FunctionSet2DConstEps_1>(southBoundary, ConditionType2D::DirichletSouth, xmin, xmin, h);
    applyConditionWest<FunctionSet2DConstEps_1>(westBoundary, ConditionType2D::DirichletWest, xmin, xmin, h);
    applyConditionEast<FunctionSet2DConstEps_1>(eastBoundary, ConditionType2D::DirichletEast, xmin, xmax, h);

    Poisson2D solver(rho, GridStep(h));

    solver.solverSteps = Steps(params.steps);
    solver.maxCalls = SolverCalls(params.multigridCalls);
    solver.setMultigridCycle(params.methodType);
    solver.epsilonC = PhysicalConstant(FunctionSet2DConstEps_1::eps());

    solver.setCondition(westBoundary, ConditionType2D::DirichletWest);
    solver.setCondition(eastBoundary, ConditionType2D::DirichletEast);
    solver.setCondition(northBoundary, ConditionType2D::DirichletNorth);
    solver.setCondition(southBoundary, ConditionType2D::DirichletSouth);

    auto time = solveWithTimer(solver);
    checkAndPrintResult(solver, FunctionSet2DConstEps_1::solution, params.methodType, xmin, xmin, params.error, time);
    cout << endl;
}

INSTANTIATE_TEST_CASE_P(NonHomogeneusNeumann,
                        Poisson2DTestSuite3,
                        ::testing::Values(
                                //xmin, xmax, nx, solver steps, mg calls, mg type, max error
                                TestParams{0, 1, 63, 5, 3, MultigridCycle::VCycle, 1e-4},
                                TestParams{0, 1, 255, 2, 4, MultigridCycle::WCycle, 1e-4},
                                TestParams{0, 1, 255, 4, 3, MultigridCycle::FullMultigrid, 1e-4}
                        ));

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct Poisson2DSetConditionTestSuite: Test
{
    Poisson2DSetConditionTestSuite(): rho(4, 5), objectUnderTest(rho, GridStep(0.1))
    {}

    void assertEveryConditionAsDirichlet()
    {
        ASSERT_EQ(ConditionType2D::DirichletNorth, objectUnderTest.getNorthCondition());
        ASSERT_EQ(ConditionType2D::DirichletSouth, objectUnderTest.getSouthCondition());
        ASSERT_EQ(ConditionType2D::DirichletWest, objectUnderTest.getWestCondition());
        ASSERT_EQ(ConditionType2D::DirichletEast, objectUnderTest.getEastCondition());
    }

    Matrix2D rho;
    Poisson2D objectUnderTest;
};

TEST_F(Poisson2DSetConditionTestSuite, shallBeEmptyByDefault)
{
    Matrix2D expectedValues(4, 5);
    ASSERT_EQ(expectedValues, objectUnderTest.phi);
}

TEST_F(Poisson2DSetConditionTestSuite, shallSetNorthDirichlet)
{
    Matrix2D expectedValues(4, 5);
    Array1D northBoundary(5);
    for(size_t i = 0; i < 5; ++i)
    {
        expectedValues(3, i) = i+1;
        northBoundary[i] = i+1;
    }

    objectUnderTest.setCondition(northBoundary, ConditionType2D::DirichletNorth);

    ASSERT_EQ(expectedValues, objectUnderTest.phi);
    assertEveryConditionAsDirichlet();
    //printMatrix2D(expectedValues);
}

TEST_F(Poisson2DSetConditionTestSuite, shallSetSouthDirichlet)
{
    Matrix2D expectedValues(4, 5);
    Array1D southBoundary(5);
    for(size_t i = 0; i < 5; ++i)
    {
        expectedValues(0, i) = i+1;
        southBoundary[i] = i+1;
    }

    objectUnderTest.setCondition(southBoundary, ConditionType2D::DirichletSouth);

    ASSERT_EQ(expectedValues, objectUnderTest.phi);
    assertEveryConditionAsDirichlet();
    //printMatrix2D(expectedValues);
}

TEST_F(Poisson2DSetConditionTestSuite, shallSetWestDirichlet)
{
    Matrix2D expectedValues(4, 5);
    Array1D westBoundary(4);
    for(size_t i = 0; i < 4; ++i)
    {
        expectedValues(i, 0) = i+1;
        westBoundary[i] = i+1;
    }

    objectUnderTest.setCondition(westBoundary, ConditionType2D::DirichletWest);

    ASSERT_EQ(expectedValues, objectUnderTest.phi);
    assertEveryConditionAsDirichlet();
}

TEST_F(Poisson2DSetConditionTestSuite, shallSetEastDirichlet)
{
    Matrix2D expectedValues(4, 5);
    Array1D easrBoundary(4);
    for(size_t i = 0; i < 4; ++i)
    {
        expectedValues(i, 4) = i+1;
        easrBoundary[i] = i+1;
    }

    objectUnderTest.setCondition(easrBoundary, ConditionType2D::DirichletEast);

    ASSERT_EQ(expectedValues, objectUnderTest.phi);
    assertEveryConditionAsDirichlet();
    //printMatrix2D(expectedValues);
}

TEST_F(Poisson2DSetConditionTestSuite, shallSetNorthNeumann)
{
    Matrix2D expectedValues(4, 5);
    Array1D northBoundary(5);
    for(size_t i = 0; i < 5; ++i)
    {
        expectedValues(3, i) = i+1;
        northBoundary[i] = i+1;
    }

    objectUnderTest.setCondition(northBoundary, ConditionType2D::NeumannNorth);

    ASSERT_EQ(expectedValues, objectUnderTest.phi);
    ASSERT_EQ(ConditionType2D::NeumannNorth, objectUnderTest.getNorthCondition());
    ASSERT_EQ(ConditionType2D::DirichletSouth, objectUnderTest.getSouthCondition());
    ASSERT_EQ(ConditionType2D::DirichletWest, objectUnderTest.getWestCondition());
    ASSERT_EQ(ConditionType2D::DirichletEast, objectUnderTest.getEastCondition());
    //printMatrix2D(expectedValues);
}

TEST_F(Poisson2DSetConditionTestSuite, shallSetSouthNeumann)
{
    Matrix2D expectedValues(4, 5);
    Array1D southBoundary(5);
    for(size_t i = 0; i < 5; ++i)
    {
        expectedValues(0, i) = i+1;
        southBoundary[i] = i+1;
    }

    objectUnderTest.setCondition(southBoundary, ConditionType2D::NeumannSouth);

    ASSERT_EQ(expectedValues, objectUnderTest.phi);
    ASSERT_EQ(ConditionType2D::DirichletNorth, objectUnderTest.getNorthCondition());
    ASSERT_EQ(ConditionType2D::NeumannSouth, objectUnderTest.getSouthCondition());
    ASSERT_EQ(ConditionType2D::DirichletWest, objectUnderTest.getWestCondition());
    ASSERT_EQ(ConditionType2D::DirichletEast, objectUnderTest.getEastCondition());
    //printMatrix2D(expectedValues);
}

TEST_F(Poisson2DSetConditionTestSuite, shallSetWestNeumann)
{
    Matrix2D expectedValues(4, 5);
    Array1D westBoundary(4);
    for(size_t i = 0; i < 4; ++i)
    {
        expectedValues(i, 0) = i+1;
        westBoundary[i] = i+1;
    }

    objectUnderTest.setCondition(westBoundary, ConditionType2D::NeumannWest);

    ASSERT_EQ(expectedValues, objectUnderTest.phi);
    ASSERT_EQ(ConditionType2D::DirichletNorth, objectUnderTest.getNorthCondition());
    ASSERT_EQ(ConditionType2D::DirichletSouth, objectUnderTest.getSouthCondition());
    ASSERT_EQ(ConditionType2D::NeumannWest, objectUnderTest.getWestCondition());
    ASSERT_EQ(ConditionType2D::DirichletEast, objectUnderTest.getEastCondition());
    //printMatrix2D(expectedValues);
}

TEST_F(Poisson2DSetConditionTestSuite, shallSetEastNeumann)
{
    Matrix2D expectedValues(4, 5);
    Array1D easrBoundary(4);
    for(size_t i = 0; i < 4; ++i)
    {
        expectedValues(i, 4) = i+1;
        easrBoundary[i] = i+1;
    }

    objectUnderTest.setCondition(easrBoundary, ConditionType2D::NeumannEast);

    ASSERT_EQ(expectedValues, objectUnderTest.phi);
    ASSERT_EQ(ConditionType2D::DirichletNorth, objectUnderTest.getNorthCondition());
    ASSERT_EQ(ConditionType2D::DirichletSouth, objectUnderTest.getSouthCondition());
    ASSERT_EQ(ConditionType2D::DirichletWest, objectUnderTest.getWestCondition());
    ASSERT_EQ(ConditionType2D::NeumannEast, objectUnderTest.getEastCondition());
    //printMatrix2D(expectedValues);
}

TEST_F(Poisson2DSetConditionTestSuite, shallSetThreeNeumannBoundaries)
{
    Matrix2D expectedValues(4, 5);
    Array1D eastAndWestBoundary(4, 1);
    Array1D southBoundary(5, 1);
    for(size_t i = 0; i < 4; ++i)
    {
        expectedValues(i, 0) = 1;
        expectedValues(i, 4) = 1;
    }
    for(size_t i = 0; i < 5; ++i)
    {
        expectedValues(0, i) = 1;
    }

    objectUnderTest.setCondition(southBoundary, ConditionType2D::NeumannSouth);
    objectUnderTest.setCondition(eastAndWestBoundary, ConditionType2D::NeumannWest);
    objectUnderTest.setCondition(eastAndWestBoundary, ConditionType2D::NeumannEast);

    ASSERT_EQ(expectedValues, objectUnderTest.phi);
    ASSERT_EQ(ConditionType2D::DirichletNorth, objectUnderTest.getNorthCondition());
    ASSERT_EQ(ConditionType2D::NeumannSouth, objectUnderTest.getSouthCondition());
    ASSERT_EQ(ConditionType2D::NeumannWest, objectUnderTest.getWestCondition());
    ASSERT_EQ(ConditionType2D::NeumannEast, objectUnderTest.getEastCondition());
}

TEST_F(Poisson2DSetConditionTestSuite, shallThrowLogicErrorWhenAllBoundariesSetToNeumann)
{
    Array1D eastAndWestBoundary(4, 1);
    Array1D northAndSouthBoundary(5, 1);

    objectUnderTest.setCondition(northAndSouthBoundary, ConditionType2D::NeumannNorth);
    objectUnderTest.setCondition(northAndSouthBoundary, ConditionType2D::NeumannSouth);
    objectUnderTest.setCondition(eastAndWestBoundary, ConditionType2D::NeumannWest);
    objectUnderTest.setCondition(eastAndWestBoundary, ConditionType2D::NeumannEast);

    ASSERT_THROW(objectUnderTest.solve(), logic_error);
}

}
