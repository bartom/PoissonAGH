#include "PoissonAGH/Poisson1D.hpp"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace PoissonAGH
{
using namespace ::testing;

constexpr DimensionSize PROBLEM_SIZE = DimensionSize(7);
constexpr GridStep GRID_STEP = GridStep(0.1);
constexpr double DEFAULT_TOLERANCE = 1e-9;
const Array1D RHO{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7}; // NOLINT
const Array1D EMPTY_ARRAY(PROBLEM_SIZE); // NOLINT
const Array1D ZERO_ARRAY(0); // NOLINT

template <typename Poisson1DType>
class Poisson1DCommonTestSuite : public Test
{
public:
    void assertCorrectSetUp() const
    {
        ASSERT_FALSE(this->m_objectUnderTest->isSolved());
        ASSERT_THAT(this->m_objectUnderTest->phi, SizeIs(PROBLEM_SIZE));
        ASSERT_THAT(this->m_objectUnderTest->rho, SizeIs(PROBLEM_SIZE));
        ASSERT_EQ(this->m_objectUnderTest->boundary.left, PhysicalConstant(0.));
        ASSERT_EQ(this->m_objectUnderTest->boundary.right, PhysicalConstant(0.));
        ASSERT_THAT(this->m_objectUnderTest->phi, Pointwise(FloatNear(DEFAULT_TOLERANCE), EMPTY_ARRAY));
    }

protected:
    std::unique_ptr<Poisson1DType> m_objectUnderTest;

};

using AllPoisson1DTypes = Types<
    Poisson1D<ConditionType1D::DirichletDirichlet>,
    Poisson1D<ConditionType1D::DirichletNeumann>,
    Poisson1D<ConditionType1D::NeumannDirichlet>,
    Poisson1D<ConditionType1D::Periodic>,
    Poisson1D<ConditionType1D::DirichletDirichlet, EpsilonType::Varying>,
    Poisson1D<ConditionType1D::DirichletNeumann, EpsilonType::Varying>,
    Poisson1D<ConditionType1D::NeumannDirichlet, EpsilonType::Varying>,
    Poisson1D<ConditionType1D::Periodic, EpsilonType::Varying>>;

TYPED_TEST_CASE(Poisson1DCommonTestSuite, AllPoisson1DTypes);

TYPED_TEST(Poisson1DCommonTestSuite, shouldConstructWithRhoArray)
{
    this->m_objectUnderTest = std::make_unique<TypeParam>(RHO, GRID_STEP);

    this->assertCorrectSetUp();
    ASSERT_THAT(this->m_objectUnderTest->rho, Pointwise(FloatNear(DEFAULT_TOLERANCE), RHO));
}

TYPED_TEST(Poisson1DCommonTestSuite, shouldThrowWhenConstructedWithNegativeStepForRhoArray)
{
    constexpr GridStep NEGATIVE_STEP{-0.1};

    ASSERT_THROW(std::make_unique<TypeParam>(EMPTY_ARRAY, NEGATIVE_STEP), PoissonError);
}

TYPED_TEST(Poisson1DCommonTestSuite, shouldThrowWhenConstructedWithArrayWithLenghtZero)
{
    ASSERT_THROW(std::make_unique<TypeParam>(ZERO_ARRAY, GRID_STEP), PoissonError);
}

TYPED_TEST(Poisson1DCommonTestSuite, shouldThrowWhenTriedToSolveWithIncorrectPhiSize)
{
    this->m_objectUnderTest = std::make_unique<TypeParam>(RHO, GRID_STEP);
    this->m_objectUnderTest->phi = {23};

    ASSERT_THROW(this->m_objectUnderTest->solve(), PoissonError);
}

TYPED_TEST(Poisson1DCommonTestSuite, shouldThrowWhenTriedToSolveWithIncorrectRhoSize)
{
    this->m_objectUnderTest = std::make_unique<TypeParam>(RHO, GRID_STEP);
    this->m_objectUnderTest->rho = {42};

    ASSERT_THROW(this->m_objectUnderTest->solve(), PoissonError);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename Poisson1DType>
class Poisson1DConstEpsTestSuite : public Test
{
public:
    Poisson1DConstEpsTestSuite():
        m_objectUnderTest(RHO, GRID_STEP)
    {}

protected:
    Poisson1DType m_objectUnderTest;
};

using ConstEpsTypes = Types<
        Poisson1D<ConditionType1D::DirichletDirichlet>,
        Poisson1D<ConditionType1D::DirichletNeumann>,
        Poisson1D<ConditionType1D::NeumannDirichlet>,
        Poisson1D<ConditionType1D::Periodic>>;

TYPED_TEST_CASE(Poisson1DConstEpsTestSuite, ConstEpsTypes);

TYPED_TEST(Poisson1DConstEpsTestSuite, shouldSetPhysicalConstatntAsEpsilonType)
{
    auto result = std::is_same<PhysicalConstant, decltype(this->m_objectUnderTest.epsilon)>::value;
    ASSERT_TRUE(result);
    ASSERT_EQ(this->m_objectUnderTest.epsilon, PhysicalConstant(0.));
}

TYPED_TEST(Poisson1DConstEpsTestSuite, shouldSetDefaultValueForEpsilonWhenTriedToSolveWithoutSettingValue)
{
    this->m_objectUnderTest.solve();

    ASSERT_FLOAT_EQ(this->m_objectUnderTest.epsilon, DEFAULT_EPSILON);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename Poisson1DType>
class Poisson1DVaryingEpsTestSuite : public Test
{
public:
    Poisson1DVaryingEpsTestSuite():
            m_objectUnderTest(RHO, GRID_STEP)
    {}

protected:
    Poisson1DType m_objectUnderTest;
};

using VaryingEpsTypes = Types<
        Poisson1D<ConditionType1D::DirichletDirichlet, EpsilonType::Varying>,
        Poisson1D<ConditionType1D::DirichletNeumann, EpsilonType::Varying>,
        Poisson1D<ConditionType1D::NeumannDirichlet, EpsilonType::Varying>,
        Poisson1D<ConditionType1D::Periodic, EpsilonType::Varying>>;

TYPED_TEST_CASE(Poisson1DVaryingEpsTestSuite , VaryingEpsTypes);

TYPED_TEST(Poisson1DVaryingEpsTestSuite, shouldSetArray1DAsEpsilonType)
{
    auto result = std::is_same<Array1D, decltype(this->m_objectUnderTest.epsilon)>::value;
    ASSERT_TRUE(result);
    ASSERT_TRUE(this->m_objectUnderTest.epsilon.empty());
}

TYPED_TEST(Poisson1DVaryingEpsTestSuite, shouldThrowWhenTriedToSolveWithoutSettingEpsilon)
{
    ASSERT_THROW(
        try
        {
            this->m_objectUnderTest.solve();
        }
        catch (const PoissonError& e)
        {
            ASSERT_STREQ(e.what(), "Incorrect size of epsilon array!");
            throw e;
        },
    PoissonError);
}

}


