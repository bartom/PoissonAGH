#include "TestTools3D.hpp"
#include "PoissonAGH/Poisson3D.hpp"

namespace PoissonAGH
{
using namespace std;
using namespace testing;

namespace
{

struct TestParams
{
    double xmin;
    double xmax;
    size_t nx;
    size_t steps;
    size_t maxSteps;
    MultigridCycle methodType;
    double error;
    double residualVariability;

    ConditionType3D front;
    ConditionType3D back;
    ConditionType3D up;
    ConditionType3D down;
    ConditionType3D left;
    ConditionType3D right;
};

TestParams createParams(double xmin, double xmax, size_t nx, size_t steps, size_t maxSteps,
                        MultigridCycle method, double error, double residualVariability,
                        ConditionType3D front = ConditionType3D::DirichletFront,
                        ConditionType3D back  = ConditionType3D::DirichletBack,
                        ConditionType3D up    = ConditionType3D::DirichletUp,
                        ConditionType3D down  = ConditionType3D::DirichletDown,
                        ConditionType3D left  = ConditionType3D::DirichletLeft,
                        ConditionType3D right = ConditionType3D::DirichletRight)
{
    return TestParams{xmin, xmax, nx, steps, maxSteps, method, error, residualVariability, front, back, up, down, left, right};
}

class FunctionSet3DConstEps_0
{
public:
    static double density(double x, double y, double z)
    {
        return 2*( (6*x*x-1)*y*y*(y*y-1)*z*z*(z*z-1) +
                   (6*y*y-1)*x*x*(x*x-1)*z*z*(z*z-1) +
                   (6*z*z-1)*x*x*(x*x-1)*y*y*(y*y-1)
        );
    }

    static double solution(double x, double y, double z)
    {
        return -(x*x-x*x*x*x)*(y*y*y*y-y*y)*(z*z-z*z*z*z);
    }

    // required by applying function but not used
    static double dervX(double, double, double)
    {
        return 0;
    }

    static double dervY(double, double, double)
    {
        return 0;
    }

    static double dervZ(double, double, double)
    {
        return 0;
    }

};

}
class Poisson3DTestSuite1: public TestWithParam<TestParams>
{
};

TEST_P(Poisson3DTestSuite1, shallSolveProblem)
{
    TestParams params = GetParam();
    double xmin = params.xmin;
    double xmax = params.xmax;
    size_t nx = params.nx;
    double h = getH(xmin, xmax, nx);

    Matrix2D front(params.nx+2);
    Matrix2D back(params.nx+2);
    Matrix2D up(params.nx+2);
    Matrix2D down(params.nx+2);
    Matrix2D left(params.nx+2);
    Matrix2D right(params.nx+2);

    Matrix3D rho(nx + 2);

    fillMatrix3DWithFunction(rho, FunctionSet3DConstEps_0::density, xmin, xmin, xmin, h);
    applyConditionsFront<FunctionSet3DConstEps_0>(front, params.front, xmin, xmin, xmin, h);
    applyConditionsBack<FunctionSet3DConstEps_0>(back, params.back, xmax, xmin, xmin, h);
    applyConditionsLeft<FunctionSet3DConstEps_0>(left, params.left, xmin, xmin, xmin, h);
    applyConditionsRight<FunctionSet3DConstEps_0>(right, params.right, xmin, xmax, xmin, h);
    applyConditionsUp<FunctionSet3DConstEps_0>(up, params.up, xmin, xmin, xmax, h);
    applyConditionsDown<FunctionSet3DConstEps_0>(down, params.down, xmin, xmin, xmin, h);

    Poisson3D solver(std::move(rho), GridStep(h));

    solver.solverSteps = Steps(params.steps);
    solver.maxCalls = SolverCalls(params.maxSteps);
    solver.setMultigridCycle(params.methodType);
    solver.epsilonC = PhysicalConstant{1};
    solver.residualVariability = params.residualVariability;

    solver.setCondition(front, ConditionType3D::DirichletFront);
    solver.setCondition(back, ConditionType3D::DirichletBack);
    solver.setCondition(up, ConditionType3D::DirichletUp);
    solver.setCondition(down, ConditionType3D::DirichletDown);
    solver.setCondition(left, ConditionType3D::DirichletLeft);
    solver.setCondition(right, ConditionType3D::DirichletRight);

    EXPECT_EQ(ConditionType3D::DirichletBack, solver.getBackCondition());
    EXPECT_EQ(ConditionType3D::DirichletUp, solver.getUpCondition());
    EXPECT_EQ(ConditionType3D::DirichletDown, solver.getDownCondition());
    EXPECT_EQ(ConditionType3D::DirichletLeft, solver.getLeftCondition());
    EXPECT_EQ(ConditionType3D::DirichletRight, solver.getRightCondition());
    EXPECT_EQ(ConditionType3D::DirichletFront, solver.getFrontCondition());

    solver.solve();

    checkResult<FunctionSet3DConstEps_0>(solver.phi, xmin, xmin, xmin, h, params.error);
}

INSTANTIATE_TEST_CASE_P(HomogeneusDirichlet,
                        Poisson3DTestSuite1,
                        ::testing::Values(
                                //xmin, xmax, nx, solver steps, mg calls, mg type, max error
                                createParams(0, 1, 63, 2, 1, MultigridCycle::FullMultigrid, 1e-4, 500),
                                createParams(0, 1, 63, 2, 7, MultigridCycle::VCycle, 1e-4, 500),
                                createParams(0, 1, 63, 2, 7, MultigridCycle::WCycle, 1e-4, 500),
                                createParams(-1, 1, 127, 2, 2, MultigridCycle::FullMultigrid, 1e-4, 500),
                                createParams(-1, 1, 127, 2, 7, MultigridCycle::VCycle, 1e-4, 500),
                                createParams(-1, 1, 127, 2, 7, MultigridCycle::WCycle, 1e-4, 500),
                                createParams(0, 2, 255, 2, 6, MultigridCycle::FullMultigrid, 1e-3, 500),
                                createParams(0, 2, 255, 2, 7, MultigridCycle::VCycle, 1e-3, 500),
                                createParams(0, 2, 255, 2, 7, MultigridCycle::WCycle, 1e-3, 500),
                                createParams(-2, 0, 255, 2, 6, MultigridCycle::FullMultigrid, 1e-3, 500),
                                createParams(-2, 0, 255, 2, 7, MultigridCycle::VCycle, 1e-3, 500),
                                createParams(-2, 0, 255, 2, 7, MultigridCycle::WCycle, 1e-3, 500)

                                // Very RAM-demanding test!
                                //createParams(-2, 2, 511, 3, 6, Poisson3D::MultigridCycle::FullMultigrid, 1e-3)
                        ));


class Poisson3DTestSuite2: public TestWithParam<TestParams>
{
};

TEST_P(Poisson3DTestSuite2, shallSolveProblem)
{
    TestParams params = GetParam();
    double xmin = params.xmin;
    double xmax = params.xmax;
    size_t nx = params.nx;
    double h = getH(xmin, xmax, nx);

    Matrix2D front(params.nx+2);
    Matrix2D back(params.nx+2);
    Matrix2D up(params.nx+2);
    Matrix2D down(params.nx+2);
    Matrix2D left(params.nx+2);
    Matrix2D right(params.nx+2);

    Matrix3D rho(nx + 2);

    fillMatrix3DWithFunction(rho, FunctionSet3DConstEps_1::density, xmin, xmin, xmin, h);
    applyConditionsFront<FunctionSet3DConstEps_1>(front, params.front, xmin, xmin, xmin, h);
    applyConditionsBack<FunctionSet3DConstEps_1>(back, params.back, xmax, xmin, xmin, h);
    applyConditionsLeft<FunctionSet3DConstEps_1>(left, params.left, xmin, xmin, xmin, h);
    applyConditionsRight<FunctionSet3DConstEps_1>(right, params.right, xmin, xmax, xmin, h);
    applyConditionsUp<FunctionSet3DConstEps_1>(up, params.up, xmin, xmin, xmax, h);
    applyConditionsDown<FunctionSet3DConstEps_1>(down, params.down, xmin, xmin, xmin, h);

    Poisson3D solver(std::move(rho), GridStep(h));

    solver.solverSteps = Steps(params.steps);
    solver.maxCalls = SolverCalls(params.maxSteps);
    solver.setMultigridCycle(params.methodType);
    solver.epsilonC = PhysicalConstant(FunctionSet3DConstEps_1::eps());

    solver.setCondition(front, params.front);
    solver.setCondition(back, params.back);
    solver.setCondition(up, params.up);
    solver.setCondition(down, params.down);
    solver.setCondition(left, params.left);
    solver.setCondition(right, params.right);

    solver.solve();

    checkResult<FunctionSet3DConstEps_1>(solver.phi, xmin, xmin, xmin, h, params.error);
}

INSTANTIATE_TEST_CASE_P(NonHomogeneusNeumannConstEps,
                        Poisson3DTestSuite2,
                        ::testing::Values(
                        createParams(-0.4, 0.6, 63, 1, 10, MultigridCycle::FullMultigrid, 1e-4, 1),
                        createParams(-0.4, 0.6, 63, 1, 10, MultigridCycle::VCycle, 1e-4, 1),
                        createParams(-0.4, 0.6, 63, 1, 10, MultigridCycle::WCycle, 1e-4, 1),

                        createParams(-0.4, 0.6, 63, 2, 20, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::NeumannFront),
                        createParams(-0.4, 0.6, 63, 2, 20, MultigridCycle::VCycle, 1e-4, 1,
                                     ConditionType3D::NeumannFront),
                        createParams(-0.4, 0.6, 63, 2, 20, MultigridCycle::WCycle, 1e-4, 1,
                                     ConditionType3D::NeumannFront),
                        createParams(-0.4, 0.6, 63, 2, 20, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::DirichletFront, ConditionType3D::NeumannBack),
                        createParams(-0.4, 0.6, 63, 3, 20, MultigridCycle::VCycle, 1e-4, 1,
                                     ConditionType3D::DirichletFront, ConditionType3D::NeumannBack),
                        createParams(-0.4, 0.6, 63, 2, 20, MultigridCycle::WCycle, 1e-4, 1,
                                     ConditionType3D::DirichletFront, ConditionType3D::NeumannBack),
                        createParams(-0.4, 0.6, 63, 2, 20, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::NeumannUp),
                        createParams(-0.4, 0.6, 63, 3, 20, MultigridCycle::VCycle, 1e-4, 1,
                                     ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::NeumannUp),
                        createParams(-0.4, 0.6, 63, 2, 20, MultigridCycle::WCycle, 1e-4, 1,
                                     ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::NeumannUp),
                        createParams(-0.4, 0.6, 63, 2, 20, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                     ConditionType3D::NeumannDown),
                        createParams(-0.4, 0.6, 63, 2, 20, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                     ConditionType3D::DirichletDown, ConditionType3D::NeumannLeft),
                        createParams(-0.4, 0.6, 63, 2, 20, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                     ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight),

                        createParams(-0.4, 0.6, 63, 3, 30, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::DirichletUp,
                                     ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                        createParams(-0.4, 0.6, 63, 3, 30, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::NeumannFront, ConditionType3D::DirichletBack, ConditionType3D::NeumannUp,
                                     ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                        createParams(-0.4, 0.6, 63, 3, 30, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::NeumannFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                     ConditionType3D::NeumannDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                        createParams(-0.4, 0.6, 63, 3, 30, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::NeumannFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                     ConditionType3D::DirichletDown, ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight),
                        createParams(-0.4, 0.6, 63, 3, 30, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::NeumannFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                     ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight),

                        createParams(-0.4, 0.6, 63, 3, 30, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                     ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                        createParams(-0.4, 0.6, 63, 3, 30, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::DirichletUp,
                                     ConditionType3D::NeumannDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                        createParams(-0.4, 0.6, 63, 3, 30, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::DirichletUp,
                                     ConditionType3D::DirichletDown, ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight),
                        createParams(-0.4, 0.6, 63, 3, 30, MultigridCycle::FullMultigrid, 1e-4, 1,
                                     ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::DirichletUp,
                                     ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight),

                        createParams(-0.4, 0.6, 63, 4, 35, MultigridCycle::FullMultigrid, 1e-4, 1e-2,
                                     ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                     ConditionType3D::NeumannDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                        createParams(-0.4, 0.6, 63, 4, 35, MultigridCycle::FullMultigrid, 1e-4, 1e-2,
                                     ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                     ConditionType3D::DirichletDown, ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight),
                        createParams(-0.4, 0.6, 63, 4, 35, MultigridCycle::FullMultigrid, 1e-4, 1e-2,
                                     ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                     ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight),

                        createParams(-0.4, 0.6, 63, 4, 35, MultigridCycle::FullMultigrid, 1e-4, 1e-2,
                                     ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                     ConditionType3D::NeumannDown, ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight),
                        createParams(-0.4, 0.6, 63, 4, 45, MultigridCycle::FullMultigrid, 1e-4, 1e-2,
                                     ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                     ConditionType3D::NeumannDown, ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight)
                        ));

class Poisson3DTestSuite3: public TestWithParam<TestParams>
{
};

TEST_P(Poisson3DTestSuite3, shallSolveProblem)
{
    TestParams params = GetParam();
    double xmin = params.xmin;
    double xmax = params.xmax;
    size_t nx = params.nx;
    double h = getH(xmin, xmax, nx);

    Matrix2D front(params.nx+2);
    Matrix2D back(params.nx+2);
    Matrix2D up(params.nx+2);
    Matrix2D down(params.nx+2);
    Matrix2D left(params.nx+2);
    Matrix2D right(params.nx+2);

    Matrix3D rho(nx + 2);
    Matrix3D eps(nx + 2);

    fillMatrix3DWithFunction(rho, FunctionSet3DVarEps_1::density, xmin, xmin, xmin, h);
    applyConditionsFront<FunctionSet3DVarEps_1>(front, params.front, xmin, xmin, xmin, h);
    applyConditionsBack<FunctionSet3DVarEps_1>(back, params.back, xmax, xmin, xmin, h);
    applyConditionsLeft<FunctionSet3DVarEps_1>(left, params.left, xmin, xmin, xmin, h);
    applyConditionsRight<FunctionSet3DVarEps_1>(right, params.right, xmin, xmax, xmin, h);
    applyConditionsDown<FunctionSet3DVarEps_1>(down, params.down, xmin, xmin, xmin, h);
    applyConditionsUp<FunctionSet3DVarEps_1>(up, params.up, xmin, xmin, xmax, h);
    fillMatrix3DWithFunction(eps, FunctionSet3DVarEps_1::eps, xmin, xmin, xmin, h);

    Poisson3D solver(std::move(rho), GridStep(h));

    solver.setEpsilonMatrix(std::move(eps));
    solver.solverSteps = Steps(params.steps);
    solver.maxCalls = SolverCalls(params.maxSteps);
    solver.setMultigridCycle(params.methodType);

    solver.setCondition(front, params.front);
    solver.setCondition(back, params.back);
    solver.setCondition(up, params.up);
    solver.setCondition(down, params.down);
    solver.setCondition(left, params.left);
    solver.setCondition(right, params.right);

    solver.solve();

    checkResult<FunctionSet3DVarEps_1>(solver.phi, xmin, xmin, xmin, h, params.error);
}

INSTANTIATE_TEST_CASE_P(NonHomogeneusNeumannVarEps,
                        Poisson3DTestSuite3,
                        ::testing::Values(
                                createParams(-0.4, 0.6, 63, 2, 25, MultigridCycle::FullMultigrid, 1e-4, 1),
                                createParams(-0.4, 0.6, 63, 1, 25, MultigridCycle::VCycle, 1e-4, 1),
                                createParams(-0.4, 0.6, 63, 1, 25, MultigridCycle::WCycle, 1e-4, 1),

                                createParams(-0.4, 0.6, 63, 3, 50, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::NeumannFront),
                                createParams(-0.4, 0.6, 63, 2, 50, MultigridCycle::VCycle, 1e-4, 1,
                                             ConditionType3D::NeumannFront),
                                createParams(-0.4, 0.6, 63, 2, 50, MultigridCycle::WCycle, 1e-4, 1,
                                             ConditionType3D::NeumannFront),
                                createParams(-0.4, 0.6, 63, 2, 50, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::DirichletFront, ConditionType3D::NeumannBack),
                                createParams(-0.4, 0.6, 63, 3, 50, MultigridCycle::VCycle, 1e-4, 1,
                                             ConditionType3D::DirichletFront, ConditionType3D::NeumannBack),
                                createParams(-0.4, 0.6, 63, 2, 50, MultigridCycle::WCycle, 1e-4, 1,
                                             ConditionType3D::DirichletFront, ConditionType3D::NeumannBack),
                                createParams(-0.4, 0.6, 63, 2, 50, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::NeumannUp),
                                createParams(-0.4, 0.6, 63, 3, 50, MultigridCycle::VCycle, 1e-4, 1,
                                             ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::NeumannUp),
                                createParams(-0.4, 0.6, 63, 2, 50, MultigridCycle::WCycle, 1e-4, 1,
                                             ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::NeumannUp),
                                createParams(-0.4, 0.6, 63, 3, 50, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                             ConditionType3D::NeumannDown),
                                createParams(-0.4, 0.6, 63, 3, 60, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                             ConditionType3D::DirichletDown, ConditionType3D::NeumannLeft),
                                createParams(-0.4, 0.6, 63, 2, 50, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::DirichletFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                             ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight),

                                createParams(-0.4, 0.6, 63, 3, 60, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::DirichletUp,
                                             ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                                createParams(-0.4, 0.6, 63, 3, 60, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::NeumannFront, ConditionType3D::DirichletBack, ConditionType3D::NeumannUp,
                                             ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                                createParams(-0.4, 0.6, 63, 5, 90, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::NeumannFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                             ConditionType3D::NeumannDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                                createParams(-0.4, 0.6, 63, 5, 90, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::NeumannFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                             ConditionType3D::DirichletDown, ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight),
                                createParams(-0.4, 0.6, 63, 3, 60, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::NeumannFront, ConditionType3D::DirichletBack, ConditionType3D::DirichletUp,
                                             ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight),

                                createParams(-0.4, 0.6, 63, 3, 60, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                             ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                                createParams(-0.4, 0.6, 63, 4, 90, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::DirichletUp,
                                             ConditionType3D::NeumannDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                                createParams(-0.4, 0.6, 63, 4, 90, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::DirichletUp,
                                             ConditionType3D::DirichletDown, ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight),
                                createParams(-0.4, 0.6, 63, 3, 60, MultigridCycle::FullMultigrid, 1e-4, 1,
                                             ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::DirichletUp,
                                             ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight),

                                createParams(-0.4, 0.6, 63, 4, 100, MultigridCycle::FullMultigrid, 1e-4, 1e-2,
                                             ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                             ConditionType3D::NeumannDown, ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight),
                                createParams(-0.4, 0.6, 63, 4, 100, MultigridCycle::FullMultigrid, 1e-4, 1e-2,
                                             ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                             ConditionType3D::DirichletDown, ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight),
                                createParams(-0.4, 0.6, 63, 4, 100, MultigridCycle::FullMultigrid, 1e-4, 1e-2,
                                             ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                             ConditionType3D::DirichletDown, ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight),

                                createParams(-0.4, 0.6, 63, 4, 200, MultigridCycle::WCycle, 1e-4, 1e-2,
                                             ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                             ConditionType3D::NeumannDown, ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight),
                                createParams(-0.4, 0.6, 63, 4, 200, MultigridCycle::WCycle, 2e-4, 1e-3,
                                             ConditionType3D::NeumannFront, ConditionType3D::NeumannBack, ConditionType3D::NeumannUp,
                                             ConditionType3D::NeumannDown, ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight)
                        ));

}
