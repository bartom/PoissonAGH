#include "TestTools3D.hpp"

namespace PoissonAGH
{
using namespace testing;
using namespace std;
using namespace utils;

namespace
{
struct ConditionsPack3D
{
    ConditionType3D front;
    ConditionType3D back;
    ConditionType3D up;
    ConditionType3D down;
    ConditionType3D left;
    ConditionType3D right;
    size_t iters;
    double error;
};
}

class SolverStrategy3DTestSuite1: public TestWithParam<ConditionsPack3D>
{};

TEST_P(SolverStrategy3DTestSuite1, shallSolveProblem)
{
    ConditionsPack3D params = GetParam();

    double xmin = -0.4;
    double xmax = 0.6;
    size_t nx = 63;
    double h = getH(xmin, xmax, nx);

    Matrix3D rho(nx+2);
    Matrix3D phi(nx+2);

    fillMatrix3DWithFunction(rho, FunctionSet3DConstEps_1::density, xmin, xmin, xmin, h);
    applyConditionsFront<FunctionSet3DConstEps_1>(phi, params.front, xmin, xmin, xmin, h);
    applyConditionsBack<FunctionSet3DConstEps_1>(phi, params.back, xmax, xmin, xmin, h);
    applyConditionsLeft<FunctionSet3DConstEps_1>(phi, params.left, xmin, xmin, xmin, h);
    applyConditionsRight<FunctionSet3DConstEps_1>(phi, params.right, xmin, xmax, xmin, h);
    applyConditionsDown<FunctionSet3DConstEps_1>(phi, params.down, xmin, xmin, xmin, h);
    applyConditionsUp<FunctionSet3DConstEps_1>(phi, params.up, xmin, xmin, xmax, h);

    auto solver = SolverGSStrategy3DFactory::createSolverConstEps(params.front, params.back, params.up, params.down,
                                                                  params.left, params.right, phi, rho, h,
                                                                  FunctionSet3DConstEps_1::eps());

    for(size_t i = 0; i < params.iters; ++i)
    {
        solver->solve();
    }
    solver->findResidual(rho);

    checkResult<FunctionSet3DConstEps_1>(phi, xmin, xmin, xmin, h, params.error);
    checkResidual(rho, solver->findSumResidual(), -1605);
}

INSTANTIATE_TEST_CASE_P(NonHomogeneusNeumannConstEps,
                        SolverStrategy3DTestSuite1,
                        ::testing::Values(
                               ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               4000, 1e-4},

                               ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                              ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                              ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                              6000, 1e-4},

                               ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::NeumannBack,
                                              ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                              ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                              6000, 1e-4},

                               ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::DirichletBack,
                                              ConditionType3D::NeumannUp, ConditionType3D::DirichletDown,
                                              ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                              6000, 1e-4},

                               ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::DirichletBack,
                                              ConditionType3D::DirichletUp, ConditionType3D::NeumannDown,
                                              ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                              6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::NeumannBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::NeumannUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::NeumannDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::NeumannDown, ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight, 12000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::NeumannUp, ConditionType3D::NeumannDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight,
                                               18000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::NeumannUp, ConditionType3D::NeumannDown,
                                               ConditionType3D::NeumannLeft, ConditionType3D::NeumannRight,
                                               20000, 1e-4}
));

class SolverStrategy3DTestSuite2: public TestWithParam<ConditionsPack3D>
{};

TEST_P(SolverStrategy3DTestSuite2, shallSolveProblem)
{
    ConditionsPack3D params = GetParam();

    double xmin = -0.4;
    double xmax = 0.6;
    size_t nx = 63;
    double h = getH(xmin, xmax, nx);

    Matrix3D rho(nx+2);
    Matrix3D phi(nx+2);
    Matrix3D eps(nx+2);

    fillMatrix3DWithFunction(rho, FunctionSet3DVarEps_1::density, xmin, xmin, xmin, h);
    applyConditionsFront<FunctionSet3DVarEps_1>(phi, params.front, xmin, xmin, xmin, h);
    applyConditionsBack<FunctionSet3DVarEps_1>(phi, params.back, xmax, xmin, xmin, h);
    applyConditionsLeft<FunctionSet3DVarEps_1>(phi, params.left, xmin, xmin, xmin, h);
    applyConditionsRight<FunctionSet3DVarEps_1>(phi, params.right, xmin, xmax, xmin, h);
    applyConditionsDown<FunctionSet3DVarEps_1>(phi, params.down, xmin, xmin, xmin, h);
    applyConditionsUp<FunctionSet3DVarEps_1>(phi, params.up, xmin, xmin, xmax, h);
    fillMatrix3DWithFunction(eps, FunctionSet3DVarEps_1::eps, xmin, xmin, xmin, h);

    auto solver = SolverGSStrategy3DFactory::createSolverVarEps(params.front, params.back, params.up, params.down,
                                                                params.left, params.right, phi, rho, h, eps);

    for(size_t i = 0; i < params.iters; ++i)
    {
        solver->solve();
    }
    solver->findResidual(rho);

    checkResult<FunctionSet3DVarEps_1>(phi, xmin, xmin, xmin, h, params.error);
    checkResidual(rho, solver->findSumResidual(), -20103);
}

INSTANTIATE_TEST_CASE_P(NonHomogeneusNeumannVarEps,
                        SolverStrategy3DTestSuite2,
                        ::testing::Values(
                                ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               4200, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::NeumannBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               6200, 1e-4},

                                ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::NeumannUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::NeumannDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::DirichletFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight,
                                               6000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::NeumannBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               7000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::NeumannUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               8000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::NeumannDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::DirichletRight,
                                               8500, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::NeumannLeft, ConditionType3D::DirichletRight,
                                               8000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::DirichletDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight,
                                               9000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::DirichletUp, ConditionType3D::NeumannDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight,
                                               12000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::NeumannUp, ConditionType3D::NeumannDown,
                                               ConditionType3D::DirichletLeft, ConditionType3D::NeumannRight,
                                               18000, 1e-4},

                                ConditionsPack3D{ConditionType3D::NeumannFront, ConditionType3D::DirichletBack,
                                               ConditionType3D::NeumannUp, ConditionType3D::NeumannDown,
                                               ConditionType3D::NeumannLeft, ConditionType3D::NeumannRight,
                                               20000, 1e-4}
));

}
