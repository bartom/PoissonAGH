#pragma once
#include <iostream>
#include <cmath>
#include <algorithm>
#include <numeric>
#include "PoissonAGH/Utils/PTypes.hpp"
#include "PoissonAGH/Utils/PMatrix.hpp"

/**
 * @addtogroup PoissonAGH
 * @{
 */
namespace PoissonAGH
{
constexpr PhysicalConstant DEFAULT_EPSILON = PhysicalConstant(0.25 / M_PI);

std::string toString(MultigridCycle cycle);
std::ostream& operator<<(std::ostream& os, MultigridCycle cycle);

constexpr double roundPositive(double val)
{
    return val - static_cast<unsigned long>(val) >= 0.5 ? val+1 : val;
}

constexpr DimensionSize calculateDimensionSize(AxisRange axis, GridStep step)
{
    return DimensionSize(static_cast<size_t>(
         roundPositive((axis.max - axis.min) / step.value())
    ));
}

constexpr GridStep calculateGridStep(AxisRange axis, DimensionSize size)
{
    return GridStep(
         (axis.max - axis.min) / static_cast<double>(size.value())
    );
}

template <typename T, typename U>
bool areAllEqual(const T& x, const U& y)
{
    return x == y;
}

template <typename T, typename U, typename... Var>
bool areAllEqual(const T& x, const U& y, const Var&... args)
{
    return (x == y) && areAllEqual(y, args...);
}

void fillArrayWith1DFunction(Array1D& array, Function1D function, GridStep step,
        double rangeStart, size_t staringIndex = 0u);

/**
 * Simple function for checking if given number is power of 2
 * @param number Given number
 * @return bool
 */
inline bool isPowerOf2(size_t number)
{
    return (number & (number - 1)) == 0;
}

inline double sqr(double x)
{
    return x*x;
}

}
/** @} End of group */
