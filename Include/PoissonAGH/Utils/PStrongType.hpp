#pragma once

#include <memory>

namespace PoissonAGH
{

template <typename T, template <typename> class ExtensionType>
class Crtp
{
public:
    T& recuring() { return static_cast<T&>(*this); }
    const T& recuring() const { return static_cast<const T&>(*this); }

private:
    Crtp() = default;
    friend ExtensionType<T>;
};

template <typename T, typename Tag, template <typename> class... Properties>
class StrongType : public Properties<StrongType<T, Tag, Properties...>>...
{
public:
    constexpr explicit StrongType(const T& value) noexcept: m_value(value)
    {}

    constexpr explicit StrongType(T&& value) noexcept: m_value(std::move(value))
    {}

    constexpr T& value() { return m_value; }
    constexpr const T& value() const { return m_value; }

private:
    T m_value;
};

template <typename T>
struct Comparable: public Crtp<T, Comparable>
{
    bool operator==(const T& rhs) const
    {
        return this->recuring().value() == rhs.value();
    }
};

template <typename T>
struct PreIncrementable: public Crtp<T, PreIncrementable>
{
    T& operator++()
    {
        ++(this->recuring().value());
        return this->recuring();
    }
};

template <typename T>
struct LtComparable : public Crtp<T, LtComparable>
{
    bool operator<(const T& rhs) const
    {
        return this->recuring().value() < rhs.value();
    }
};

template <typename T>
struct AddAssignable : public Crtp<T, AddAssignable>
{
    T& operator+=(const T& rhs)
    {
        this->recuring().value() += rhs.value();
        return this->recuring();
    }
};

template <typename Destination>
struct ImplicitlyConvertibleTo
{
    template <typename T>
    struct Impl: Crtp<T, Impl>
    {
        operator Destination() const // NOLINT
        {
            return this->recuring().value();
        }
    };
};


}
