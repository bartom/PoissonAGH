#pragma once

#include "PoissonAGH/Utils/PStrongType.hpp"
#include <vector>
#include <chrono>

namespace PoissonAGH
{

enum class EpsilonType
{
    Constant,
    Varying
};

namespace ConditionTags1D
{

struct DirichletDirichlet{};
struct DirichletNeumann{};
struct NeumannDirichlet{};
struct Periodic{};

}

enum class ConditionType1D
{
    DirichletDirichlet,
    DirichletNeumann,
    NeumannDirichlet,
    Periodic
};

enum class MultigridCycle
{
    VCycle,
    WCycle,
    FCycle,
    FullMultigrid
};

using DimensionSize = StrongType<size_t, struct DimensionSizeTag,
                                 Comparable,
                                 ImplicitlyConvertibleTo<size_t>::Impl>;

using GridStep = StrongType<double, struct GridStepTag,
                            Comparable,
                            ImplicitlyConvertibleTo<double>::Impl>;

using PhysicalConstant = StrongType<double, struct PhysicalConstantTag,
                                    Comparable,
                                    ImplicitlyConvertibleTo<double>::Impl>;

using Steps = StrongType<size_t, struct StepsTag,
                         Comparable,
                         LtComparable,
                         AddAssignable,
                         PreIncrementable>;

using SolverCalls = StrongType<size_t, struct SolverCallsTag,
                               Comparable,
                               LtComparable,
                               PreIncrementable>;


using Function1D = double(*)(double);
using Function2D = double(*)(double, double);
using Function3D = double(*)(double, double, double);

using Clock = std::chrono::high_resolution_clock;
using Milis = std::chrono::milliseconds;

struct Boundary1D
{
    PhysicalConstant left{0.};
    PhysicalConstant right{0.};
};

/////////////////////////////////////////////////////////////////////////

class PoissonError : public std::runtime_error
{
public:
    explicit PoissonError(const char* errorMessage):
        runtime_error(errorMessage)
    {}

    explicit PoissonError(const std::string& errorMessage):
            runtime_error(errorMessage)
    {}
};

struct AxisRange
{
    double min = 0;
    double max = 0;

    constexpr auto tie() const { return std::tie(min, max); }
    constexpr double span() const { return max - min; }
};

constexpr bool operator==(const AxisRange& lhs, const AxisRange& rhs)
{
    return lhs.tie() == rhs.tie();
}

constexpr bool operator!=(const AxisRange& lhs, const AxisRange& rhs)
{
    return not (lhs.tie() == rhs.tie());
}

}
