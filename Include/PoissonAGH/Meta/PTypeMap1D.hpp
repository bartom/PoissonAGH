#pragma once

#include "PoissonAGH/Utils/PTypes.hpp"
#include "PoissonAGH/Solver/PSolver1D.hpp"

namespace PoissonAGH
{

template <ConditionType1D, EpsilonType>
struct TypeMap1D;

template <>
struct TypeMap1D<ConditionType1D::DirichletDirichlet, EpsilonType::Constant>
{
    using solverType = Solver::Poisson1DSolverConstEps<ConditionTags1D::DirichletDirichlet>;
};

template <>
struct TypeMap1D<ConditionType1D::DirichletNeumann, EpsilonType::Constant>
{
    using solverType = Solver::Poisson1DSolverConstEps<ConditionTags1D::DirichletNeumann>;
};

template <>
struct TypeMap1D<ConditionType1D::NeumannDirichlet, EpsilonType::Constant>
{
    using solverType = Solver::Poisson1DSolverConstEps<ConditionTags1D::NeumannDirichlet>;
};

template <>
struct TypeMap1D<ConditionType1D::Periodic, EpsilonType::Constant>
{
    using solverType = Solver::Poisson1DSolverConstEps<ConditionTags1D::Periodic>;
};

template <>
struct TypeMap1D<ConditionType1D::DirichletDirichlet, EpsilonType::Varying>
{
    using solverType = Solver::Poisson1DSolverVaryingEps<ConditionTags1D::DirichletDirichlet>;
};

template <>
struct TypeMap1D<ConditionType1D::DirichletNeumann, EpsilonType::Varying>
{
    using solverType = Solver::Poisson1DSolverVaryingEps<ConditionTags1D::DirichletNeumann>;
};

template <>
struct TypeMap1D<ConditionType1D::NeumannDirichlet, EpsilonType::Varying>
{
    using solverType = Solver::Poisson1DSolverVaryingEps<ConditionTags1D::NeumannDirichlet>;
};

template <>
struct TypeMap1D<ConditionType1D::Periodic, EpsilonType::Varying>
{
    using solverType = Solver::Poisson1DSolverVaryingEps<ConditionTags1D::Periodic>;
};

}