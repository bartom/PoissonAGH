#pragma once
#include "PoissonAGH/Solver/SolverStrategy3D.hpp"

/**
 * @addtogroup PoissonAGH
 * @{
 */
namespace PoissonAGH
{
using utils::Solver3DPtr;

#ifdef WIP
/**
 * @class Poisson3D
 * @brief 3D multigrid Poisson equation solver.
 */
//class Poisson3D : public Poisson
class Poisson3D : public WithGridStep<Poisson3D>,
                  public WithXAxis<Poisson3D>,
                  public WithYAxis<Poisson3D>,
                  public WithZAxis<Poisson3D>,
                  public Solveable<Poisson3D>
{
using MultigridFunction3D = void(Poisson3D::*)(Matrix3D&, const Matrix3D&, double, const Matrix3D&,
                                               const Solver3DPtr&);

public:

    Poisson3D(AllAxisRange axes, GridStep h):
        WithGridStep(h),
        WithXAxis(calculateDimensionSize(axes, h)),
        WithYAxis(axes, calculateDimensionSize(axes, h)),
        WithZAxis(axes, calculateDimensionSize(axes, h)),
        phi(m_nX + 2), rho(m_nX+2)
    {}

    Poisson3D(AllAxisRange axes, DimensionSize n):
        WithGridStep(calculateGridStep(axes, n)),
        WithXAxis(n),
        WithYAxis(axes, n),
        WithZAxis(axes, n),
        phi(m_nX + 2), rho(m_nX+2)
    {}

    Poisson3D(AxisXRange axisX, AxisYRange axisY, AxisZRange axisZ, GridStep h):
        WithGridStep(h),
        WithXAxis(calculateDimensionSize(axisX, h)),
        WithYAxis(axisY, calculateDimensionSize(axisY, h)),
        WithZAxis(axisZ, calculateDimensionSize(axisZ, h)),
        phi(m_nX+2, m_nY+2, m_nZ+2), rho(m_nX+2, m_nY+2, m_nZ+2)
    {}

    Poisson3D(Matrix3D rhoMatrix, GridStep h):
        WithGridStep(h),
        WithXAxis( DimensionSize(rhoMatrix.rows()-2)),
        WithYAxis({0, 0}, DimensionSize(rhoMatrix.cols()-2)),
        WithZAxis({0, 0}, DimensionSize(rhoMatrix.slices()-2)),
        phi(rhoMatrix.rows(), rhoMatrix.cols(), rhoMatrix.slices()), rho(std::move(rhoMatrix))
    {}

    /**
     * Function for solving equation.
     */
    void solve();

    /**
     * Setter for absolute permittivity matrix, should be used if
     * permittivity is not constant.
     *
     * @tparam Matrix3DType Matrix3D from library, template made for universal reference's sake
     * @param epsilon Matrix3D of absolute permitivity values
     */
    void setEpsilonMatrix(Matrix3D epsilon)
    {
        if(epsilon.rows() != rho.rows() or epsilon.cols() != rho.cols() or epsilon.slices() != rho.slices())
        {
            throw std::logic_error("Epsilon matrix with wrong size passed!");
        }
        epsilonMatrixGiven = true;
        epsilonMatrix = std::move(epsilon);
    }

    /**
     * Setter for boundary conditions. Could be Dirichlet or Neumann.
     *
     * @param values 2D vector of values on specyfic boundary
     * @param condition Enum indicating side of cube and condition type
     */
    void setCondition(const Matrix2D& values, ConditionType3D condition);

    /**
     * Setter for multigrid solver type. Available types:
     * VCycle
     * WCycle
     * Full Multigrid
     *
     * @param cycle Solver cycle
     */
    void setMultigridCycle(MultigridCycle cycle);

    Matrix3D phi; /// 3D matrix with initial phi values (zeros) and solution after calling solve
    Matrix3D rho; /// 3D matrix with charge density
    Matrix3D epsilonMatrix;
    PhysicalConstant epsilonC{0.25/M_PI}; /// absolute permittivity [au] (defualt: vacuum permittivity = 0.25/pi [au])
    SolverCalls maxCalls{10};  /// amount of whole multigrid method calls
    Steps solverSteps{2};     /// amount of simple internal iterative solver calls
    Steps solverStepsTotal{0};
    double residualVariability = 1e-3;

    ConditionType3D getFrontCondition() const { return frontCondition; }
    ConditionType3D getBackCondition() const { return backCondition; }
    ConditionType3D getUpCondition() const { return upCondition; }
    ConditionType3D getDownCondition() const { return downCondition; }
    ConditionType3D getLeftCondition() const { return leftCondition; }
    ConditionType3D getRightCondition() const { return rightCondition; }

private:
    MultigridFunction3D multigridFunction = &Poisson3D::wCycle;

    bool fullMultigridOn = false;
    bool epsilonMatrixGiven = false;

    ConditionType3D frontCondition = ConditionType3D::DirichletFront;
    ConditionType3D backCondition = ConditionType3D::DirichletBack;
    ConditionType3D upCondition = ConditionType3D::DirichletUp;
    ConditionType3D downCondition = ConditionType3D::DirichletDown;
    ConditionType3D leftCondition = ConditionType3D::DirichletLeft;
    ConditionType3D rightCondition = ConditionType3D::DirichletRight;

    void init() const;
    void vCycle(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix, const Solver3DPtr& solverStrategy);
    void wCycle(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix, const Solver3DPtr& solverStrategy);
    void fullMultigrid(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix,
                       const Solver3DPtr& solverStrategy);

    static void prolongate(Matrix3D& v, const Matrix3D& V);
    static void restrict(Matrix3D& R, const Matrix3D& r);
    static void restrictSimple(Matrix3D& R, const Matrix3D& r);
    static void restrictBorders(Matrix3D& R, const Matrix3D& r);

    template<bool W_CYCLE>
    void basicCycle(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix, const Solver3DPtr& solverStrategy);
    void fCycle(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix, const Solver3DPtr& solverStrategy);

    void checkIfSizeMatchRows(size_t size) const;
    void checkIfSizeMatchCols(size_t size) const;
    void checkIfSizeMatchSlices(size_t size) const;

    void setBoundaryValuesFrontOrBack(const Matrix2D& values, size_t index);
    void setBoundaryValuesUpOrDown(const Matrix2D& values, size_t index);
    void setBoundaryValuesLeftOrRight(const Matrix2D& values, size_t index);

    static void interpolateHalfs(Matrix3D& v, const Matrix3D& V, size_t I, size_t J, size_t K);
    static void interpolateQuaters(Matrix3D& v, const Matrix3D& V, size_t I, size_t J, size_t K);
    static void interpolateEighths(Matrix3D& v, const Matrix3D& V, size_t I, size_t J, size_t K);

    Solver3DPtr createSolver3DPtr(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix) noexcept;

};
#endif

}
/** @} End of group */

