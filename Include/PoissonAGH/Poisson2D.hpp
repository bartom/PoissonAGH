#pragma once
#include "PoissonAGH/Solver/SolverStrategy2D.hpp"

/**
 * @addtogroup PoissonAGH
 * @{
 */
namespace PoissonAGH
{
using utils::Solver2DPtr;

#ifdef WIP
/**
 * @class Poisson2D
 * @brief 2D multigrid Poisson equation solver.
 */
class Poisson2D : public WithGridStep<Poisson2D>,
                  public WithXAxis<Poisson2D>,
                  public WithYAxis<Poisson2D>,
                  public Solveable<Poisson2D>
{
using MultigridFunction2D = void(Poisson2D::*)(Matrix2D&, const Matrix2D&, GridStep, const Matrix2D&, const Solver2DPtr&);

public:
    /**
     * Constructor for square grid
     */
    Poisson2D(AllAxisRange axes, GridStep h):
        WithGridStep(h),
        WithXAxis(calculateDimensionSize(axes, h)),
        WithYAxis(axes, calculateDimensionSize(axes, h)),
        phi(m_nX+2), rho(m_nX+2)
    {}

    Poisson2D(AllAxisRange axes, DimensionSize n):
        WithGridStep(calculateGridStep(axes, n)),
        WithXAxis(n),
        WithYAxis(axes, n),
        phi(m_nX+2), rho(m_nX+2)
    {}

    Poisson2D(AxisXRange axisX, AxisYRange axisY, GridStep h):
        WithGridStep(h),
        WithXAxis(calculateDimensionSize(axisX, h)),
        WithYAxis(axisY, calculateDimensionSize(axisY, h)),
        phi(m_nX+2, m_nY+2), rho(m_nX+2, m_nY+2)
    {}

    Poisson2D(Matrix2D rhoMatrix, GridStep h):
            WithGridStep(h),
            WithXAxis(DimensionSize(rhoMatrix.rows()-2)),
            WithYAxis({0, 0}, DimensionSize(rhoMatrix.cols()-2)),
            phi(rhoMatrix.rows(), rhoMatrix.cols()), rho(std::move(rhoMatrix))
    {}

    /**
     * Function for solving equation.
     */
    void solve();

    /**
     * Setter for absolute permittivity matrix, should be used if
     * permittivity is not constant.
     *
     * @tparam Matrix2DType Matrix2D from library
     * @param epsilon Matrix2D of absolute permitivity values
     */
    void setEpsilonMatrix(Matrix2D epsilon)
    {
        if(epsilon.rows() != rho.rows() or epsilon.cols() != rho.cols())
        {
            throw std::logic_error("Epsilon matrix with wrong size passed!");
        }
        epsilonMatrixGiven = true;
        epsilonMatrix = std::move(epsilon);
    }

    /**
     * Setter for boundary conditions. Could be Dirichlet or Neumann.
     *
     * @param values Vector of values on specyfic boundary
     * @param condition Enum indicating side of grid and condition type
     */
    void setCondition(const Array1D& values, ConditionType2D condition);

    /**
     * Setter for multigrid solver cycle. Available types:
     * VCycle
     * WCycle
     * Full Multigrid
     *
     * @param cycle Solver cycle
     */
    void setMultigridCycle(MultigridCycle cycle);

    Matrix2D phi; /// 2D matrix with initial phi values (zeros) and solution after calling solve
    Matrix2D rho; /// 2D matrix with charge density

    Matrix2D epsilonMatrix;
    PhysicalConstant epsilonC{0.25/M_PI}; /// absolute permittivity [au] (defualt: vacuum permittivity = 0.25/pi [au])

    Steps solverSteps{2};     /// amount of simple internal iterative solver calls
    Steps solverStepsTotal{0};

    SolverCalls maxCalls{10};  /// amount of whole multigrid method calls
    double residualDelta = 1e-3;

    ConditionType2D getNorthCondition() const { return northCondition; }
    ConditionType2D getSouthCondition() const { return southCondition; }
    ConditionType2D getWestCondition() const { return westCondition; }
    ConditionType2D getEastCondition() const { return eastCondition; }

private:
    MultigridFunction2D multigridFunction = &Poisson2D::wCycle;

    bool fullMultigridOn = false;
    bool epsilonMatrixGiven = false;

    ConditionType2D northCondition = ConditionType2D::DirichletNorth;
    ConditionType2D southCondition = ConditionType2D::DirichletSouth;
    ConditionType2D westCondition = ConditionType2D::DirichletWest;
    ConditionType2D eastCondition = ConditionType2D::DirichletEast;

    void init();
    void vCycle(Matrix2D& u, const Matrix2D& f, GridStep h, const Matrix2D& epsMatrix,
                const Solver2DPtr& solverStrategy);
    void wCycle(Matrix2D& u, const Matrix2D& f, GridStep h, const Matrix2D& epsMatrix,
                const Solver2DPtr& solverStrategy);
    void fCycle(Matrix2D& u, const Matrix2D& f, GridStep h, const Matrix2D& epsMatrix,
                const Solver2DPtr& solverStrategy);
    void fullMultigrid(Matrix2D& u, const Matrix2D& f, GridStep h, const Matrix2D& epsMatrix,
                       const Solver2DPtr& solverStrategy);

    static void prolongate(Matrix2D& v, const Matrix2D& V);
    static void restrict(Matrix2D& R, const Matrix2D& r);
    static void restrictSimple(Matrix2D& R, const Matrix2D& r);
    static void restrictBorders(Matrix2D& R, const Matrix2D& r);

    template<bool W_CYCLE>
    void basicCycle(Matrix2D& u, const Matrix2D& f, GridStep h, const Matrix2D& epsMatrix,
                    const Solver2DPtr& solverStrategy);

    void checkIfSizeMatchRows(size_t size);
    void checkIfSizeMatchCols(size_t size);

    void setBoundaryValuesNorthOrSouth(const Array1D& values, size_t index);
    void setBoundaryValuesWestOrEast(const Array1D& values, size_t index);

    Solver2DPtr createSolver2DPtr(Matrix2D& u, const Matrix2D& f, GridStep h, const Matrix2D& epsMatrix) noexcept;

};
#endif

}
/** @} End of group */

