#pragma once

#include "PoissonAGH/Utils/PTypes.hpp"
#include "PoissonAGH/Utils/PMatrix.hpp"
#include <algorithm>
#include <type_traits>

namespace PoissonAGH
{

namespace Solver
{

namespace Detail
{

class PConstEpsInitializer
{
public:
    static void modifyBoundary(Sparse& eqMatrix, Array1D& phi, Boundary1D boundary,
                               GridStep h, const PhysicalConstant& eps,
                               ConditionTags1D::DirichletDirichlet);

    static void modifyBoundary(Sparse& eqMatrix, Array1D& phi, Boundary1D boundary,
                               GridStep h, const PhysicalConstant& eps,
                               ConditionTags1D::DirichletNeumann);

    static void modifyBoundary(Sparse& eqMatrix, Array1D& phi, Boundary1D boundary,
                               GridStep h, const PhysicalConstant& eps,
                               ConditionTags1D::NeumannDirichlet);

    static void modifyBoundary(Sparse& eqMatrix, Array1D& phi, Boundary1D boundary,
                               GridStep h, const PhysicalConstant& eps,
                               ConditionTags1D::Periodic);
};

/////////////////////////////////////////////////////////////////////////////////////////////////

class PVarEpsInitializer
{
public:
    static void modifyBoundary(Sparse& eqMatrix, Array1D& phi, Boundary1D boundary,
                               GridStep h, const Array1D& epsilonArr,
                               ConditionTags1D::DirichletDirichlet);

    static void modifyBoundary(Sparse& eqMatrix, Array1D& phi, Boundary1D boundary,
                               GridStep h, const Array1D& epsilonArr,
                               ConditionTags1D::DirichletNeumann);

    static void modifyBoundary(Sparse& eqMatrix, Array1D& phi, Boundary1D boundary,
                               GridStep h, const Array1D& epsilonArr,
                               ConditionTags1D::NeumannDirichlet);

    static void modifyBoundary(Sparse& eqMatrix, Array1D& phi, Boundary1D boundary,
                               GridStep h, const Array1D& epsilonArr,
                               ConditionTags1D::Periodic);
};

/////////////////////////////////////////////////////////////////////////////////////////////////

class Algorithm1D
{
public:
    static void thomasAlgorithm(const Sparse& eqMatrix, Array1D& f);

    static void shermanMorrison(Sparse& eqMatrix, Array1D& f);

    template<typename ConditionType>
    typename std::enable_if_t<!std::is_same<ConditionType, ConditionTags1D::Periodic>::value, void>
    static solveEquation(Sparse& eqMatrix, Array1D& f)
    {
        thomasAlgorithm(eqMatrix, f);
    }

    template<typename ConditionType>
    typename std::enable_if_t<std::is_same<ConditionType, ConditionTags1D::Periodic>::value, void>
    static solveEquation(Sparse& eqMatrix, Array1D& f)
    {
        shermanMorrison(eqMatrix, f);
    }
};

}

/////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
class Poisson1DSolver : public Crtp<T, Poisson1DSolver>
{
public:
    template <class U = T>
    void init(Sparse& eqMatrix, Array1D& rho, Array1D& phi, Boundary1D boundary,
              GridStep h, typename U::EpsilonType& eps)
    {
        this->recuring().initImpl(eqMatrix, rho, phi, boundary, h, eps);
    }

    void solve(Sparse& eqMatrix, Array1D &solution)
    {
        this->recuring().solveImpl(eqMatrix, solution);
    }
};

/////////////////////////////////////////////////////////////////////////////////////////////////

template <typename ConditionType>
class Poisson1DSolverConstEps : public Poisson1DSolver<Poisson1DSolverConstEps<ConditionType>>
{
public:
    using EpsilonType = PhysicalConstant;
    using BConditionType = ConditionType;

    void initImpl(Sparse& eqMatrix, Array1D& rho, Array1D& phi, Boundary1D boundary,
                  GridStep h, PhysicalConstant& eps)
    {
        initDiagonals(eqMatrix);

        auto multiplyParam = -h * h * eps;
        std::transform(std::cbegin(rho), std::cend(rho), std::begin(phi),
                       [multiplyParam](double v) { return v * multiplyParam; });

        Detail::PConstEpsInitializer::modifyBoundary(eqMatrix, phi, boundary, h, eps, ConditionType{});

        if (std::is_same<ConditionType, ConditionTags1D::Periodic>::value)
        {
            // periodic condition extends size of eqMatrix and phi by 1, so we need to do
            // the same with rho to stay consistent
            rho.push_back(0.);
        }
    }

    void solveImpl(Sparse& eqMatrix, Array1D& solution)
    {
        Detail::Algorithm1D::solveEquation<ConditionType>(eqMatrix, solution);
    }

private:
    static void initDiagonals(Sparse& eqMatrix)
    {
        std::fill(std::begin(eqMatrix.D),  std::end(eqMatrix.D), -2.);
        std::fill(std::begin(eqMatrix.DL), std::end(eqMatrix.DL), 1.);
        std::fill(std::begin(eqMatrix.DU), std::end(eqMatrix.DU), 1.);

        const auto size = eqMatrix.DU.size();
        eqMatrix.DL[0] = eqMatrix.DU[size - 1] = 0.;
    }

};

/////////////////////////////////////////////////////////////////////////////////////////////////

template <typename ConditionType>
class Poisson1DSolverVaryingEps : public Poisson1DSolver<Poisson1DSolverVaryingEps<ConditionType>>
{
public:
    using EpsilonType = Array1D;
    using BConditionType = ConditionType;

    void initImpl(Sparse& eqMatrix, Array1D& rho, Array1D& phi, Boundary1D boundary,
                  GridStep h, Array1D& epsilonArr)
    {
        initDiagonals(eqMatrix, epsilonArr);

        auto multiplyParam = -2 * h * h;
        std::transform(std::cbegin(rho), std::cend(rho), std::begin(phi),
                       [multiplyParam](double v) { return v * multiplyParam; });

        Detail::PVarEpsInitializer::modifyBoundary(eqMatrix, phi, boundary, h, epsilonArr, ConditionType{});

        if (std::is_same<ConditionType, ConditionTags1D::Periodic>::value)
        {
            // periodic condition extends size of eqMatrix and phi by 1, so we need to do
            // the same with rho and epsilon to stay consistent
            rho.emplace_back(0.);
            epsilonArr.emplace_back(0.);
        }
    }

    void solveImpl(Sparse& eqMatrix, Array1D& solution)
    {
        Detail::Algorithm1D::solveEquation<ConditionType>(eqMatrix, solution);
    }

private:
    static void initDiagonals(Sparse& eqMatrix, const Array1D& epsilonArr)
    {
        auto size = eqMatrix.D.size();
        auto& DL = eqMatrix.DL;
        auto& D = eqMatrix.D;
        auto& DU = eqMatrix.DU;

        for(auto i = 1ul; i < size - 1; ++i)
        {
            D[i] = -(epsilonArr[i-1] + 2 * epsilonArr[i] + epsilonArr[i+1]);
            DL[i] = epsilonArr[i-1] + epsilonArr[i];
            DU[i] = epsilonArr[i] + epsilonArr[i + 1];
        }

        D[0] = -(3 * epsilonArr[0] + epsilonArr[1]);
        DU[0] = epsilonArr[0] + epsilonArr[1];
        D[size-1] = -(3 * epsilonArr[size-1] + epsilonArr[size-2]);
        DL[size-1] = epsilonArr[size-1] + epsilonArr[size-2];
    }
};

}

}

