#pragma once

#include "PoissonAGH/Utils/PTools.hpp"
#include "PoissonAGH/Utils/PMatrix.hpp"

/**
 * @addtogroup PoissonAGH
 * @{
 */
namespace PoissonAGH
{

/**
 * @class ConditionType3D
 *
 * Enum containing all possible variations of boundary conditions types (Dirichlet, Neumann), and cubic grid sides
 * (Front, Back, Up, Down, Left, Right).
 */
enum class ConditionType3D { DirichletFront, DirichletBack, DirichletUp, DirichletDown, DirichletLeft, DirichletRight,
                             NeumannFront, NeumannBack, NeumannUp, NeumannDown, NeumannLeft, NeumannRight};

/**
 * @addtogroup utils
 * @{
 */
namespace utils
{

/**
 * @class ISolverStrategy3D
 *
 * Base class for all 3D iterative solvers, which can be used as the heart of the multigrid method.
 * Besides solver, method for finding residual is also necessary.
 */
class ISolverStrategy3D
{
public:
    virtual ~ISolverStrategy3D() = default;
    /**
     * Single iterative method solver step.
     */
    virtual void solve() noexcept = 0;
    /**
     * Method for finding residual: r = f - Av.
     * Assumption is that on begin r == f
     *
     * @param r Residual to find, charge density on beginning
     */
    virtual void findResidual(Matrix3D& r) noexcept = 0;
    virtual double findSumResidual() noexcept = 0;
};

using Solver3DPtr = std::unique_ptr<ISolverStrategy3D>;

/**
 * @class IGaussSeidelSolverStrategy3D
 *
 * Base class for all red-black Gauss Seidel 3D iterative solvers for multigrid method.
 */
class IGaussSeidelSolverStrategy3D: public ISolverStrategy3D
{
public:
    ~IGaussSeidelSolverStrategy3D() override = default;

protected:
    enum Colour { RED = 0, BLACK = 1 };
    virtual void gaussSeidelOneColour(Colour colour) noexcept = 0;
};

/**
 * @class AllDirichletGSConstEps3D
 *
 * Class implementing solver with red-black Gauss Seidel method and all Dirichlet boundary conditions for constant
 * absolute permittivity.
 */
class AllDirichletGSConstEps3D: public IGaussSeidelSolverStrategy3D
{
public:
    AllDirichletGSConstEps3D(Matrix3D& u, const Matrix3D& f, double h, double eps):
        u(u), f(f), h(h), eps(eps), rows(u.rows()-2), cols(u.cols()-2), slices(u.slices()-2)
    {}
    void solve() noexcept override;
    void findResidual(Matrix3D& r) noexcept override;
    double findSumResidual() noexcept override;

private:
    Matrix3D& u;
    const Matrix3D& f;
    const double h;
    const double eps;
    const size_t rows;
    const size_t cols;
    const size_t slices;
    void gaussSeidelOneColour(Colour colour) noexcept override;
};

/**
 * @class NeumannSolverGSConstEps3D
 *
 * Class implementing solver with red-black Gauss Seidel method and one or more Neumann boundary conditions for
 * constant absolute permittivity.
 */
class NeumannSolverGSConstEps3D: public IGaussSeidelSolverStrategy3D
{
public:
    NeumannSolverGSConstEps3D(bool frontNeumann, bool backNeumann, bool upNeumann, bool downNeumann,
                              bool leftNeumann, bool rightNeumann, Matrix3D& u, const Matrix3D& f, double h, double eps);
    void solve() noexcept override;
    void findResidual(Matrix3D& r) noexcept override;
    double findSumResidual() noexcept override;

private:
    Matrix3D& u;
    const Matrix3D& f;
    const double h;
    const double eps;
    const size_t rows;
    const size_t cols;
    const size_t slices;

    double hMinDown;
    double hMaxDown;
    double hMinUp;
    double hMaxUp;
    double hMinLeft;
    double hMaxLeft;
    double hMinRight;
    double hMaxRight;
    double hMinFront;
    double hMaxFront;
    double hMinBack;
    double hMaxBack;

    void gaussSeidelOneColour(Colour colour) noexcept override;

    void solveDirichlet(Matrix3D& u, size_t i, size_t j, size_t k) const noexcept;

    void solveNeumann(Matrix3D& u, size_t i, size_t j, size_t k, double hXmin, double hXmax, double hYmin, double hYmax,
                          double hZmin, double hZmax) const noexcept;

    void findResidualDirichlet(Matrix3D& r, size_t i, size_t j, size_t k) const noexcept;

    void findResidualNeumann(Matrix3D& r, size_t i, size_t j, size_t k, double hXmin, double hXmax, double hYmin,
                             double hYmax, double hZmin, double hZmax) const noexcept;

    void findSumResidualDirichlet(double& sum, size_t i, size_t j, size_t k) const noexcept;

    void findSumResidualNeumann(double& sum, size_t i, size_t j, size_t k, double hXmin, double hXmax, double hYmin,
                                    double hYmax, double hZmin, double hZmax) const noexcept;

    template<typename F1, typename F2, typename U>
    void generalSolverTemplate(F1 nFunction, F2 dFunction, U& variable, Colour colour) const noexcept
    {
        // vertexes
        if(colour == Colour::RED)
        {
            (this->*nFunction)(variable, 1, 1, 1, hMinFront, hMaxFront, hMinLeft, hMaxLeft, hMinDown, hMaxDown);
            (this->*nFunction)(variable, 1, 1, slices, hMinFront, hMaxFront, hMinLeft, hMaxLeft, hMinUp, hMaxUp);
            (this->*nFunction)(variable, 1, cols, 1, hMinFront, hMaxFront, hMinRight, hMaxRight, hMinDown, hMaxDown);
            (this->*nFunction)(variable, 1, cols, slices, hMinFront, hMaxFront, hMinRight, hMaxRight, hMinUp, hMaxUp);

            (this->*nFunction)(variable, rows, 1, 1, hMinBack, hMaxBack, hMinLeft, hMaxLeft, hMinDown, hMaxDown);
            (this->*nFunction)(variable, rows, 1, slices, hMinBack, hMaxBack, hMinLeft, hMaxLeft, hMinUp, hMaxUp);
            (this->*nFunction)(variable, rows, cols, 1, hMinBack, hMaxBack, hMinRight, hMaxRight, hMinDown, hMaxDown);
            (this->*nFunction)(variable, rows, cols, slices, hMinBack, hMaxBack, hMinRight, hMaxRight, hMinUp, hMaxUp);
        }

        // edges
        for(size_t i = 3u-colour; i < rows; i+=2)
        {
            (this->*nFunction)(variable, i, 1, slices, 1., 1., hMinLeft, hMaxLeft, hMinUp, hMaxUp);
            (this->*nFunction)(variable, i, cols, slices, 1., 1., hMinRight, hMaxRight, hMinUp, hMaxUp);
            (this->*nFunction)(variable, i, 1, 1, 1., 1., hMinLeft, hMaxLeft, hMinDown, hMaxDown);
            (this->*nFunction)(variable, i, cols, 1, 1., 1., hMinRight, hMaxRight, hMinDown, hMaxDown);
        }

        for(size_t j = 3u-colour; j < cols; j+=2)
        {
            (this->*nFunction)(variable, 1, j, slices, hMinFront, hMaxFront, 1., 1., hMinUp, hMaxUp);
            (this->*nFunction)(variable, 1, j, 1, hMinFront, hMaxFront, 1., 1., hMinDown, hMaxDown);
            (this->*nFunction)(variable, rows, j, slices, hMinBack, hMaxBack, 1., 1., hMinUp, hMaxUp);
            (this->*nFunction)(variable, rows, j, 1, hMinBack, hMaxBack, 1., 1., hMinDown, hMaxDown);
        }

        for(size_t k = 3u-colour; k < slices; k+=2)
        {
            (this->*nFunction)(variable, 1, 1, k, hMinFront, hMaxFront, hMinLeft, hMaxLeft, 1., 1.);
            (this->*nFunction)(variable, 1, cols, k, hMinFront, hMaxFront, hMinRight, hMaxRight, 1., 1.);
            (this->*nFunction)(variable, rows, 1, k, hMinBack, hMaxBack, hMinLeft, hMaxLeft, 1., 1.);
            (this->*nFunction)(variable, rows, cols, k, hMinBack, hMaxBack, hMinRight, hMaxRight, 1., 1.);
        }

        // sides
        for(size_t i = 2; i < rows; ++i)
        {
            for(size_t k = 3 - (i + colour) % 2; k < slices; k += 2)
            {
                (this->*nFunction)(variable, i, 1, k, 1., 1., hMinLeft, hMaxLeft, 1., 1.);
                (this->*nFunction)(variable, i, cols, k, 1., 1., hMinRight, hMaxRight, 1., 1.);
            }

            for(size_t j = 3 - (i + colour) % 2; j < cols; j += 2)
            {
                (this->*nFunction)(variable, i, j, slices, 1., 1., 1., 1., hMinUp, hMaxUp);
                (this->*nFunction)(variable, i, j, 1, 1., 1., 1., 1., hMinDown, hMaxDown);
            }
        }

        for(size_t j = 2; j < cols; j++)
        {
            for(size_t k = 3 - (j + colour) % 2; k < slices; k += 2)
            {
                (this->*nFunction)(variable, 1, j, k, hMinFront, hMaxFront, 1., 1., 1., 1.);
                (this->*nFunction)(variable, rows, j, k, hMinBack, hMaxBack, 1., 1., 1., 1.);
            }
        }

        // inside
        for(size_t i = 2; i < rows; ++i)
        {
            for(size_t j = 2; j < cols; ++j)
            {
                for(size_t k = 3 - (i + j + colour) % 2; k < slices; k += 2)
                {
                    (this->*dFunction)(variable, i, j, k);
                }
            }
        }
    };
};

/**
 * @class AllDirichletGSVarEps3D
 *
 * Class implementing solver with red-black Gauss Seidel method and all Dirichlet boundary conditions for variable
 * absolute permittivity given as 3D matrix.
 */
class AllDirichletGSVarEps3D: public IGaussSeidelSolverStrategy3D
{
public:
    AllDirichletGSVarEps3D(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& eps):
            u(u), f(f), h(h), eps(eps), rows(u.rows()-2), cols(u.cols()-2), slices(u.slices()-2)
    {}
    void solve() noexcept override;
    void findResidual(Matrix3D& r) noexcept override;
    double findSumResidual() noexcept override;

private:
    void gaussSeidelOneColour(Colour colour) noexcept override;
    Matrix3D& u;
    const Matrix3D& f;
    const double h;
    const Matrix3D& eps;
    const size_t rows;
    const size_t cols;
    const size_t slices;
};

/**
 * @class NeumannSolverGSVarEps3D
 *
 * Class implementing solver with red-black Gauss Seidel method and one or more Neumann boundary conditions for
 * variable absolute permittivity gives as 3D matrix.
 */
class NeumannSolverGSVarEps3D: public IGaussSeidelSolverStrategy3D
{
    typedef double (NeumannSolverGSVarEps3D::*PartialSolver)(Matrix3D& u, size_t, size_t, size_t);
public:
    NeumannSolverGSVarEps3D(bool isFront, bool isBack, bool isUp, bool isDown, bool isLeft, bool isRight,
                            Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& eps);
    void solve() noexcept override;
    void findResidual(Matrix3D& r) noexcept override;
    double findSumResidual() noexcept override;

private:
    Matrix3D& u;
    const Matrix3D& f;
    const double h;
    const Matrix3D& eps;
    const size_t rows;
    const size_t cols;
    const size_t slices;

    PartialSolver frontDirichlet;
    PartialSolver backDirichlet;
    PartialSolver leftDirichlet;
    PartialSolver rightDirichlet;
    PartialSolver downDirichlet;
    PartialSolver upDirichlet;

    PartialSolver frontNeumann;
    PartialSolver backNeumann;
    PartialSolver leftNeumann;
    PartialSolver rightNeumann;
    PartialSolver downNeumann;
    PartialSolver upNeumann;

    void gaussSeidelOneColour(Colour colour) noexcept override;
    double getSumEps(size_t i, size_t j, size_t k) const noexcept;

    void solveDirichlet(Matrix3D& u, size_t i, size_t j, size_t k) noexcept;

    template<typename F>
    void solveNeumann(Matrix3D& u, size_t i, size_t j, size_t k, F front, F back, F left, F right, F down, F up) noexcept
    {
        u(i, j, k) = ((this->*front)(u, i, j, k) + (this->*back)(u, i, j, k) + (this->*left)(u, i, j, k) + (this->*right)(u, i, j, k)
                      + (this->*down)(u, i, j, k) + (this->*up)(u, i, j, k) + 2*h*h*f(i, j, k)) / getSumEps(i, j, k);
    }

    void findResidualDirichlet(Matrix3D& r, size_t i, size_t j , size_t k) const noexcept;

    template<typename F>
    void findResidualNeumann(Matrix3D& r, size_t i, size_t j, size_t k, F front, F back, F left, F right,
                             F down, F up) noexcept
    {
        r(i, j, k) += ((this->*front)(u, i, j, k) + (this->*back)(u, i, j, k) + (this->*left)(u, i, j, k) + (this->*right)(u, i, j, k)
                      + (this->*down)(u, i, j, k) + (this->*up)(u, i, j, k) - getSumEps(i, j, k)*u(i, j, k)) / (2*h*h);
    }

    void findSumResidualDirichlet(double& r, size_t i, size_t j , size_t k) const noexcept;

    template<typename F>
    void findSumResidualNeumann(double& r, size_t i, size_t j, size_t k, F front, F back, F left, F right,
                             F down, F up) noexcept
    {
        r += f(i, j, k) + ((this->*front)(u, i, j, k) + (this->*back)(u, i, j, k) + (this->*left)(u, i, j, k) + (this->*right)(u, i, j, k)
                       + (this->*down)(u, i, j, k) + (this->*up)(u, i, j, k) - getSumEps(i, j, k)*u(i, j, k)) / (2*h*h);
    }

    template<int s_i, int s_j, int s_k>
    double createPartialDirichletSolver(Matrix3D& u, size_t i, size_t j, size_t k) noexcept
    {
        return (eps(i+s_i, j+s_j, k+s_k)+eps(i, j, k))*u(i+s_i, j+s_j, k+s_k);
    }

    template<int s_i, int s_j, int s_k>
    double createPartialNeumannSolver(Matrix3D& u, size_t i, size_t j, size_t k) noexcept
    {
        return (eps(i+s_i, j+s_j, k+s_k)+eps(i, j, k))*((s_i+s_j+s_k)*2*h*u(i+s_i, j+s_j, k+s_k) + u(i-s_i, j-s_j, k-s_k));
    }

    template<typename F1, typename F2, typename U>
    void generalSolverTemplate(F1 nFunction, F2 dFunction, U& variable, Colour colour) noexcept
    {
        // vertexes
        if(colour == Colour::RED)
        {
            (this->*nFunction)(variable, 1, 1, 1,            frontNeumann, backDirichlet, leftNeumann, rightDirichlet, downNeumann, upDirichlet);
            (this->*nFunction)(variable, 1, 1, slices,       frontNeumann, backDirichlet, leftNeumann, rightDirichlet, downDirichlet, upNeumann);
            (this->*nFunction)(variable, 1, cols, 1,         frontNeumann, backDirichlet, leftDirichlet, rightNeumann, downNeumann, upDirichlet);
            (this->*nFunction)(variable, 1, cols, slices,    frontNeumann, backDirichlet, leftDirichlet, rightNeumann, downDirichlet, upNeumann);

            (this->*nFunction)(variable, rows, 1, 1,         frontDirichlet, backNeumann, leftNeumann, rightDirichlet, downNeumann, upDirichlet);
            (this->*nFunction)(variable, rows, 1, slices,    frontDirichlet, backNeumann, leftNeumann, rightDirichlet, downDirichlet, upNeumann);
            (this->*nFunction)(variable, rows, cols, 1,      frontDirichlet, backNeumann, leftDirichlet, rightNeumann, downNeumann, upDirichlet);
            (this->*nFunction)(variable, rows, cols, slices, frontDirichlet, backNeumann, leftDirichlet, rightNeumann, downDirichlet, upNeumann);
        }

        // edges (without vertexes)
        for(size_t i = 3u-colour; i < rows; i+=2)
        {
            (this->*nFunction)(variable, i, 1, slices,    frontDirichlet, backDirichlet, leftNeumann, rightDirichlet, downDirichlet, upNeumann);
            (this->*nFunction)(variable, i, cols, slices, frontDirichlet, backDirichlet, leftDirichlet, rightNeumann, downDirichlet, upNeumann);
            (this->*nFunction)(variable, i, 1, 1,         frontDirichlet, backDirichlet, leftNeumann, rightDirichlet, downNeumann, upDirichlet);
            (this->*nFunction)(variable, i, cols, 1,      frontDirichlet, backDirichlet, leftDirichlet, rightNeumann, downNeumann, upDirichlet);
        }

        for(size_t j = 3u-colour; j < cols; j+=2)
        {
            (this->*nFunction)(variable, 1, j, slices,    frontNeumann, backDirichlet, leftDirichlet, rightDirichlet, downDirichlet, upNeumann);
            (this->*nFunction)(variable, 1, j, 1,         frontNeumann, backDirichlet, leftDirichlet, rightDirichlet, downNeumann, upDirichlet);
            (this->*nFunction)(variable, rows, j, slices, frontDirichlet, backNeumann, leftDirichlet, rightDirichlet, downDirichlet, upNeumann);
            (this->*nFunction)(variable, rows, j, 1,      frontDirichlet, backNeumann, leftDirichlet, rightDirichlet, downNeumann, upDirichlet);
        }

        for(size_t k = 3u-colour; k < slices; k+=2)
        {
            (this->*nFunction)(variable, 1, 1, k,       frontNeumann, backDirichlet, leftNeumann, rightDirichlet, downDirichlet, upDirichlet);
            (this->*nFunction)(variable, 1, cols, k,    frontNeumann, backDirichlet, leftDirichlet, rightNeumann, downDirichlet, upDirichlet);
            (this->*nFunction)(variable, rows, 1, k,    frontDirichlet, backNeumann, leftNeumann, rightDirichlet, downDirichlet, upDirichlet);
            (this->*nFunction)(variable, rows, cols, k, frontDirichlet, backNeumann, leftDirichlet, rightNeumann, downDirichlet, upDirichlet);
        }
        // sides (without edges)
        for(size_t i = 2; i < rows; ++i)
        {
            for(size_t k = 3 - (i + colour) % 2; k < slices; k += 2)
            {
                (this->*nFunction)(variable, i, 1, k,    frontDirichlet, backDirichlet, leftNeumann, rightDirichlet, downDirichlet, upDirichlet);
                (this->*nFunction)(variable, i, cols, k, frontDirichlet, backDirichlet, leftDirichlet, rightNeumann, downDirichlet, upDirichlet);
            }

            for(size_t j = 3 - (i + colour) % 2; j < cols; j += 2)
            {
                (this->*nFunction)(variable, i, j, slices, frontDirichlet, backDirichlet, leftDirichlet, rightDirichlet, downDirichlet, upNeumann);
                (this->*nFunction)(variable, i, j, 1,      frontDirichlet, backDirichlet, leftDirichlet, rightDirichlet, downNeumann, upDirichlet);
            }
        }

        for(size_t j = 2; j < cols; j++)
        {
            for(size_t k = 3 - (j + colour) % 2; k < slices; k += 2)
            {
                (this->*nFunction)(variable, 1, j, k,    frontNeumann, backDirichlet, leftDirichlet, rightDirichlet, downDirichlet, upDirichlet);
                (this->*nFunction)(variable, rows, j, k, frontDirichlet, backNeumann, leftDirichlet, rightDirichlet, downDirichlet, upDirichlet);
            }
        }

        // inside
        for(size_t i = 2; i < rows; ++i)
        {
            for(size_t j = 2; j < cols; ++j)
            {
                for(size_t k = 3 - (i + j + colour) % 2; k < slices; k += 2)
                {
                    (this->*dFunction)(variable, i, j, k);
                }
            }
        }
    };

};

/**
 * @class SolverGSStrategy3DFactory
 *
 * Factory class for creating all types of 3D solvers.
 */
class SolverGSStrategy3DFactory
{
public:
    static Solver3DPtr createSolverConstEps(ConditionType3D front, ConditionType3D back, ConditionType3D up,
                                            ConditionType3D down, ConditionType3D left, ConditionType3D right,
                                            Matrix3D& u, const Matrix3D& f, double h, double eps);

    static Solver3DPtr createSolverVarEps(ConditionType3D front, ConditionType3D back, ConditionType3D up,
                                          ConditionType3D down, ConditionType3D left, ConditionType3D right,
                                          Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& eps);
};

}
/** @} End of group */

}
/** @} End of group */

