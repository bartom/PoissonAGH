#pragma once
#include "PoissonAGH/Utils/PTypes.hpp"
#include "PoissonAGH/Utils/PTools.hpp"
#include "PoissonAGH/Utils/PMatrix.hpp"
#include "PoissonAGH/Solver/PSolver1D.hpp"
#include "PoissonAGH/Meta/PTypeMap1D.hpp"
#include <iostream>

/**
 * @addtogroup PoissonAGH
 * @{
 */
namespace PoissonAGH{

template <typename SolverType>
class Poisson1DImpl;

template <ConditionType1D c, EpsilonType e = EpsilonType::Constant>
class Poisson1D : public Poisson1DImpl<typename TypeMap1D<c, e>::solverType>
{
public:
    Poisson1D(Array1D rhoArray, GridStep h) noexcept(false):
        Poisson1DImpl<typename TypeMap1D<c, e>::solverType>(std::move(rhoArray), h)
    {};
};

using Poisson1DConstEpsDD = Poisson1D<ConditionType1D::DirichletDirichlet>;
using Poisson1DConstEpsDN = Poisson1D<ConditionType1D::DirichletNeumann>;
using Poisson1DConstEpsND = Poisson1D<ConditionType1D::NeumannDirichlet>;
using Poisson1DConstEpsP = Poisson1D<ConditionType1D::Periodic>;
using Poisson1DVarEpsDD = Poisson1D<ConditionType1D::DirichletDirichlet, EpsilonType::Varying>;
using Poisson1DVarEpsDN = Poisson1D<ConditionType1D::DirichletNeumann, EpsilonType::Varying>;
using Poisson1DVarEpsND = Poisson1D<ConditionType1D::NeumannDirichlet, EpsilonType::Varying>;
using Poisson1DVarEpsP = Poisson1D<ConditionType1D::Periodic, EpsilonType::Varying>;

////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename SolverType>
class Poisson1DImpl
{
    using SelfType = Poisson1DImpl<SolverType>;
    using EpsilonType = typename SolverType::EpsilonType;

public:
    Poisson1DImpl(const Poisson1DImpl&) = delete;

    bool isSolved() const { return m_isSolved; }
    GridStep gridStep() const { return m_h; }

    /**
     * Solves problem.
     * Throws when given data is inconsistent or condition matrix is ill conditioned.
     */
    void solve() noexcept(false);

    Array1D phi;             /// solution vector
    Array1D rho;             /// array with charge density
    EpsilonType epsilon;

    Boundary1D boundary;
    using ConditionType = typename SolverType::BConditionType;

protected:
    Poisson1DImpl(Array1D rhoArray, GridStep h) noexcept(false);
    Poisson1DImpl(Poisson1DImpl&&) noexcept = default;
    ~Poisson1DImpl() = default;

private:
    Solver::Poisson1DSolver<SolverType> m_solver;
    Sparse m_equationMatrix;
    GridStep m_h;
    bool m_isSolved;

    void validateConstruction();
    void validateDataConsistency();
    void validateEpsilon(PhysicalConstant& epsilonC);
    void validateEpsilon(const Array1D& epsilonArr);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename SolverType>
Poisson1DImpl<SolverType>::Poisson1DImpl(Array1D rhoArray, GridStep h) noexcept(false):
    phi(rhoArray.size()),
    rho(std::move(rhoArray)),
    epsilon(0),
    m_equationMatrix(DimensionSize(phi.size())),
    m_h(h),
    m_isSolved(false)
{
    validateConstruction();
}

template <typename SolverType>
void Poisson1DImpl<SolverType>::solve() noexcept(false)
{
    validateDataConsistency();
    m_solver.init(m_equationMatrix, rho, phi, boundary, m_h, epsilon);
    m_solver.solve(m_equationMatrix, phi);
    m_isSolved = true;
}

template <typename SolverType>
void Poisson1DImpl<SolverType>::validateConstruction()
{
    if (m_h <= 0.)
    {
        throw PoissonError("Grid step cannot be less than 0!");
    }

    if (m_equationMatrix.D.size() < 4)
    {
        throw PoissonError("Problem size is too small, pass larger rho array!");
    }
}

template <typename SolverType>
void Poisson1DImpl<SolverType>::validateDataConsistency()
{
    if (not areAllEqual(phi.size(), rho.size(), m_equationMatrix.D.size()))
    {
        throw PoissonError("Incorrect data size: phi, rho or equation matrix has different size.");
    }

    validateEpsilon(epsilon);
}

template <typename SolverType>
void Poisson1DImpl<SolverType>::validateEpsilon(PhysicalConstant& epsilonC)
{
    if (epsilonC == 0u)
    {
        std::cerr << "Absolute permittivity not set, setting default value (vaccum = 0.25/pi)" << std::endl;
        epsilonC = DEFAULT_EPSILON;
    }
}

template <typename SolverType>
void Poisson1DImpl<SolverType>::validateEpsilon(const Array1D& epsilonArr)
{
    if (epsilonArr.size() != m_equationMatrix.D.size())
    {
        throw PoissonError("Incorrect size of epsilon array!");
    }
}


}
/** @} End of group */

